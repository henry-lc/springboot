package com.project.app.input.word;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hwpf.extractor.WordExtractor;

public class DocUtil {

	public static List<StringBuilder> doc2String(FileInputStream fs) throws IOException {
		StringBuilder result = new StringBuilder();
		WordExtractor re = new WordExtractor(fs);
		String[] paragraphs = re.getParagraphText();
		List<StringBuilder> lists = new ArrayList<StringBuilder>();
	
		for(String paragraph : paragraphs) {
			if(StringUtils.isBlank(paragraph)) {
				if(StringUtils.isNotBlank(result)) {
					lists.add(result);
				}
				result = new StringBuilder();
				continue;
			}else {
				result.append(paragraph);
			}	
		}
		re.close();
		return lists;
	}
 
	public static List<StringBuilder> doc2String(File file) throws IOException {
		return doc2String(new FileInputStream(file));
	}
}
