package com.project.app.webcollect;

import cn.edu.hfut.dmic.webcollector.model.CrawlDatum;
import cn.edu.hfut.dmic.webcollector.plugin.net.OkHttpRequester;
import okhttp3.Request;

public class TaobaoRequestCookie extends OkHttpRequester{

	private String userAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36";
	
	@Override
    public Request.Builder createRequestBuilder(CrawlDatum crawlDatum) {
        return super.createRequestBuilder(crawlDatum)
                .addHeader("User-Agent", userAgent);
    }
}
