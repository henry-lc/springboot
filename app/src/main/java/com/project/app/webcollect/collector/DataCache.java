package com.project.app.webcollect.collector;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantLock;

import com.project.app.entity.ExcelEntity;
import com.project.app.entity.NewsEntity;

public class DataCache {
	
	/**单利模式*/
	private  static DataCache steelCache = null;
	/**防止并发重复新建对象*/
	private static ReentrantLock lock = new ReentrantLock();

	/**记录最后爬取时间 1：废钢-调价  2：废钢-基地  3:塑料  4:有色*/
	private  Map<String,String> lastDate = new ConcurrentHashMap<String, String>();
	
	private Map<String,List<ExcelEntity>> excelMaps = new ConcurrentHashMap<String,List<ExcelEntity>>();
	
	private Map<String,List<NewsEntity>> wordMaps = new ConcurrentHashMap<String,List<NewsEntity>>();
	
	private static void syncInit(){
		lock.lock();
		if(steelCache == null){
			steelCache = new DataCache();
		}
		lock.unlock();
	}
	
	public static DataCache getInstance() {
		if(steelCache == null) {
			syncInit();
		}
		return steelCache;
	}

	public Map<String, String> getLastDate() {
		return lastDate;
	}

	public void setLastDate(Map<String, String> lastDate) {
		this.lastDate = lastDate;
	}

	public Map<String, List<ExcelEntity>> getExcelMaps() {
		return excelMaps;
	}

	public void setExcelMaps(Map<String, List<ExcelEntity>> excelMaps) {
		this.excelMaps = excelMaps;
	}

	public Map<String, List<NewsEntity>> getWordMaps() {
		return wordMaps;
	}

	public void setWordMaps(Map<String, List<NewsEntity>> wordMaps) {
		this.wordMaps = wordMaps;
	}

}
