package com.project.app.webcollect.plastic;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import com.project.app.entity.NewsEntity;
import com.project.app.webcollect.RequestCookie;

import cn.edu.hfut.dmic.webcollector.model.CrawlDatums;
import cn.edu.hfut.dmic.webcollector.model.Page;
import cn.edu.hfut.dmic.webcollector.plugin.rocks.BreadthCrawler;

/**
 * 
* @ClassName: PlasticContentCrawl 
* @Description: 获取内容 
* @author luoxiao
* @date 2019年10月17日 下午2:10:01 
*
 */
public class PlasticContentCrawl extends BreadthCrawler{
	
	public static List<NewsEntity> results = new ArrayList<NewsEntity>();

	public PlasticContentCrawl(String crawlPath, boolean autoParse) {
		super(crawlPath, autoParse);
		//设置线程数
		this.setThreads(20);
		//设置请求cookie
		this.setRequester(new RequestCookie());
	}

	@Override
	public void visit(Page page, CrawlDatums next) {
		
		Document doc =Jsoup.parse(page.doc().html().replaceAll("&nbsp;", ""));	
		
		String title = doc.select("div.width635").select("h1").get(0).text();
		String dateTime = doc.select("div.width635").select("p.center").select("span.cGray").get(0).text();
		String content = doc.select("div.width635").select("div#zhengwen").get(0).text();
		dateTime = dateTime.replace("来源：富宝资讯", "");
		int position = content.indexOf("我要订阅 ");
		if(position>-1) {
			content = content.substring(0, position);
		}
		position = content.indexOf("富宝资讯免责声明");
		if(position>-1) {
			content = content.substring(0, position);
		}
		content = content.replaceAll("  +", "").replace("(作者：富宝塑料研究小组)", "");
		SimpleDateFormat sdf = new SimpleDateFormat("MM月d日");
		String date = sdf.format(Calendar.getInstance().getTime());
		String replaced = "富宝资讯"+date+"消息";
		if(content.indexOf(replaced) > -1) {
			content = content.substring(replaced.length()+1,content.length());
		}
		NewsEntity news = new NewsEntity();
		news.setTitle(title);
		news.setDateTime(dateTime.replaceAll(" +", ""));
		news.setDescription(content);
		
		results.add(news);
	}
}
