package com.project.app.webcollect.taobao;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


import com.project.app.config.ChromeDriverConfig;
import com.project.app.entity.TaoBaoEntity;
import com.project.app.util.SpringUtils;

import cn.edu.hfut.dmic.webcollector.model.CrawlDatums;
import cn.edu.hfut.dmic.webcollector.model.Page;
import cn.edu.hfut.dmic.webcollector.plugin.rocks.BreadthCrawler;

public class TaoBaoContentCrawl extends BreadthCrawler {

	public static List<TaoBaoEntity> results = new ArrayList<TaoBaoEntity>();
	
	public TaoBaoContentCrawl(String crawlPath, boolean autoParse) {
		super(crawlPath, autoParse);
	
		this.setThreads(2);
	}

	@Override
	public void visit(Page page, CrawlDatums next) {
		ChromeDriverConfig chrome = (ChromeDriverConfig) SpringUtils.getBean(ChromeDriverConfig.class);
		System.setProperty("webdriver.chrome.driver", chrome.getPosition());
//		ChromeOptions chromeOption = new ChromeOptions();
//		chromeOption.addArguments("--headless");
		Document doc = null;
		WebDriver webDriver = new ChromeDriver();
		try {
			webDriver.get(page.url()); 
			webDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			doc = Jsoup.parse(webDriver.getPageSource());
			webDriver.quit();
		} catch(Exception e){
			throw new RuntimeException(e.getMessage());
		}finally {
			webDriver.quit();
		}
		
		String title = doc.select("h1").get(0).text();
		
		if(doc.select("div#J_Map") != null) {
			doc.select("div#J_Map").remove();
		}
		String notice =  doc.select("div#NoticeDetail").html();
		String src = "";
		if(doc.select("div.slide-bigpic") != null) {	
			Elements divImages = doc.select("div.slide-bigpic");
			for(Element div : divImages) {
				Element image = div.select("img").get(0);
				image.attr("src", image.attr("data-ks-lazyload"));
				src = src + ("https:"+image.attr("src")) + ",";
			}
			if(StringUtils.isNotBlank(src)) {
				src = src.substring(0, src.length()-1);
			}
		}
		
		
        
		
		String remindTip = doc.select("div.addition-desc").get(1).html();
		remindTip = remindTip.replace("公告详情加载中......", "");
		remindTip = remindTip.replace("标的物详情加载中......", "");
		remindTip = remindTip.replace("附件加载中......", "");
		remindTip = remindTip.replace("地图标注仅供参考，具体位置以标的物实际为准", "");
		notice = notice.replace("公告详情加载中......", "");
		notice = notice.replace("标的物详情加载中......", "");
		notice = notice.replace("附件加载中......", "");
		String content = notice + remindTip;
		TaoBaoEntity tao = new TaoBaoEntity();
		tao.setContent(content);
		tao.setTitle(title);
		tao.setUrls(src);
		results.add(tao);
	}
}
