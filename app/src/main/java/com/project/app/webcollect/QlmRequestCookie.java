package com.project.app.webcollect;

import cn.edu.hfut.dmic.webcollector.model.CrawlDatum;
import cn.edu.hfut.dmic.webcollector.plugin.net.OkHttpRequester;
import okhttp3.Request;

public class QlmRequestCookie extends OkHttpRequester{

	private String userAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36";

	@Override
    public Request.Builder createRequestBuilder(CrawlDatum crawlDatum) {
        // 这里使用的是OkHttp中的Request.Builder
        // 可以参考OkHttp的文档来修改请求头

//		String cookie = "__jsluid_h=bc6a202cfc1c8dba0513c36d560aaa9e; UM_distinctid=16e5931aae2e1-019ccad39b24ba-b363e65-1fa400-16e5931aae33df; gr_session_id_83e3b26ab9124002bae03256fc549065=f338feb4-1725-4267-bd12-537a17a97e0b; gr_session_id_83e3b26ab9124002bae03256fc549065_f338feb4-1725-4267-bd12-537a17a97e0b=true; gr_user_id=498b37b0-cc5b-4478-87d0-888fd6b419a8; Hm_lvt_0a38bdb0467f2ce847386f381ff6c0e8=1573105225,1573433176,1573455795; Hm_lvt_5dc1b78c0ab996bd6536c3a37f9ceda7=1573105225,1573433177,1573455795; cookie_insert_log=0; seo_refUrl=\"http://www.directlyaccess.com\"; qlmll_his=\",158521692,158519116,158434478,158519116,158521692,\"; nts_login_tip=1; Hm_lpvt_0a38bdb0467f2ce847386f381ff6c0e8=1573456076; qlm_username=long2039; qlm_password=8CCEfouEUEuUmB78fmmRg8foCRuUf77m; rem_login=1; fromWhereUrl=\"http://www.qianlima.com/zb/detail/20191111_158521692.html\"; Hm_lpvt_5dc1b78c0ab996bd6536c3a37f9ceda7=1573456164; JSESSIONID=792756C3CFC068782EA2100BDB51F8EC.tomcat1; seo_curUrl=\"http://wap.qianlima.com/project.jsp\"; CNZZDATA1277918875=522808377-1573101943-%7C1573453034; seo_intime=\"2019-11-11 15:10:25\"; Hm_lvt_a31e80f5423f0ff316a81cf4521eaf0d=1573456225; Hm_lpvt_a31e80f5423f0ff316a81cf4521eaf0d=1573456227; _cnzz_CV1277918875=%E6%98%AF%E5%90%A6%E7%99%BB%E5%BD%95%7C%E5%B7%B2%E7%99%BB%E5%BD%95%7C1573456228517";
        String cookie = "__jsluid_h=af1755104d375427898e3cad94426afe; gr_user_id=f515b3c5-ab7b-4171-aa04-591a2537fc8b; UM_distinctid=16ebb884bdc172-03e8ff726827c-2393f61-1fa400-16ebb884bddc45; seo_intime=\"2019-12-09 11:34:30\"; guest_id=899f2b45-e963-4540-a8a9-ae3f08ad80f7; Hm_lvt_0a38bdb0467f2ce847386f381ff6c0e8=1575104308,1576138068; gr_session_id_83e3b26ab9124002bae03256fc549065=efd177fe-7f1d-4ed9-b30c-426236a69b3c; gr_session_id_83e3b26ab9124002bae03256fc549065_efd177fe-7f1d-4ed9-b30c-426236a69b3c=true; qlm_username=17343477927; qlm_password=fp3pgmfBpCjuUBfKpuUgfCEjKjgRo7Bf; rem_login=1; bridgeid=73032595; keywordUnit=44046; keywords=%E5%8D%83%E9%87%8C%E9%A9%AC%E5%8D%83%E9%87%8C%E9%A9%AC; keywordvaluess=\"\"; laiyuan=3; Hm_lpvt_0a38bdb0467f2ce847386f381ff6c0e8=1576138247; BAIDU_SSP_lcr=https://www.baidu.com/link?url=xS4Op-qs6xoCCfZrAtgqPli82Y1HsuPporZ0QRulVxJZGQqViBAmd-BXRgku1LCC&wd=&eqid=ef634bb100042bc8000000065df1f603; Hm_lvt_5dc1b78c0ab996bd6536c3a37f9ceda7=1575858899,1575862472,1576138068,1576138251; LXB_REFER=www.baidu.com; seo_refUrl=https%3A//www.baidu.com/link%3Furl%3DxS4Op-qs6xoCCfZrAtgqPli82Y1HsuPporZ0QRulVxJZGQqViBAmd-BXRgku1LCC%26wd%3D%26eqid%3Def634bb100042bc8000000065df1f603; seo_curUrl=www.qianlima.com; qlmll_his=\",163849979,161871063,161806339,\"; fromWhereUrl=\"http://www.qianlima.com/zb/detail/20191212_163849979.html\"; Hm_lpvt_5dc1b78c0ab996bd6536c3a37f9ceda7=1576138275; JSESSIONID=iDcXFN_SOG83Hdet9grK_Wa6NSOUdnlOrCOnVPiC; CNZZDATA1277608403=668753391-1575102351-http%253A%252F%252Fwww.qianlima.com%252F%7C1576139827";
        return super.createRequestBuilder(crawlDatum)
                .addHeader("User-Agent", userAgent)
                .addHeader("Cookie", cookie);
    }
}
