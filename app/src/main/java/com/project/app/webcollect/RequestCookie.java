package com.project.app.webcollect;


import com.project.app.cookie.CookieEntity;

import cn.edu.hfut.dmic.webcollector.model.CrawlDatum;
import cn.edu.hfut.dmic.webcollector.plugin.net.OkHttpRequester;
import okhttp3.Request;

public class RequestCookie extends OkHttpRequester {

	 private String userAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36";
	 
	 @Override
     public Request.Builder createRequestBuilder(CrawlDatum crawlDatum) {
         // 这里使用的是OkHttp中的Request.Builder
         // 可以参考OkHttp的文档来修改请求头
		 if(CookieEntity.getInstance().getCookie() == null || CookieEntity.getInstance().getCookie().size()<=1 ) {
			 throw new RuntimeException("请设置Cookie!");
		 }
		 String cookie = CookieEntity.getInstance().getCookie().get(0)+";"+CookieEntity.getInstance().getCookie().get(1);

         return super.createRequestBuilder(crawlDatum)
                 .addHeader("User-Agent", userAgent)
                 .addHeader("Cookie", cookie);
     }
}
