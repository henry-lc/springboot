package com.project.app.webcollect.color;

import java.util.ArrayList;
import java.util.List;

import org.jsoup.nodes.Document;

import com.project.app.entity.NewsEntity;
import com.project.app.webcollect.RequestCookie;

import cn.edu.hfut.dmic.webcollector.model.CrawlDatums;
import cn.edu.hfut.dmic.webcollector.model.Page;
import cn.edu.hfut.dmic.webcollector.plugin.rocks.BreadthCrawler;

/**
 * 
* @ClassName: ColorContentCrawl 
* @Description: 废有色内容爬取 
* @author luoxiao
* @date 2019年10月18日 上午9:27:29 
*
 */
public class ColorContentCrawl extends BreadthCrawler{

	public static List<NewsEntity> results = new ArrayList<NewsEntity>();
	
	public ColorContentCrawl(String crawlPath, boolean autoParse) {
		super(crawlPath, autoParse);
		//设置线程数
		this.setThreads(1);
		//设置请求cookie
		this.setRequester(new RequestCookie());
	}

	@Override
	public void visit(Page page, CrawlDatums next) {
		
		Document doc = page.doc();	
		
		String title = doc.select("div.width635").select("h1").get(0).text();
		String dateTime = doc.select("div.width635").select("p.center").select("span.cGray").get(0).text();
		String content = doc.select("div.width635").select("div#zhengwen").get(0).text();
		dateTime = dateTime.replace("来源：富宝资讯", "");
		int position = content.indexOf("我要订阅 ");
		if(position>-1) {
			content = content.substring(0, position);
		}
		position = content.indexOf("富宝资讯免责声明");
		if(position>-1) {
			content = content.substring(0, position);
		}
		content = content.replaceAll("  +", "");
		if(content.indexOf("富宝资讯") == 0 && content.indexOf("：")>-1) {
			content = content.substring(content.indexOf("：")+1, content.length());
		}else if(content.indexOf("富宝资讯") == 0 && content.indexOf(":")>-1) {
			content = content.substring(content.indexOf(": 　　")+1, content.length());
		}
		if(content.indexOf("尊敬的客户：")>-1) {
			content = content.substring(content.indexOf("：")+1, content.length());
		}
		NewsEntity news = new NewsEntity();
		news.setTitle(title);
		news.setDateTime(dateTime.replaceAll(" +", ""));
		news.setDescription(content);
		
		results.add(news);
		
	}
}
