package com.project.app.webcollect.excel;

import java.util.ArrayList;
import java.util.List;

import org.jsoup.nodes.Document;

import com.project.app.webcollect.RequestCookie;

import cn.edu.hfut.dmic.webcollector.model.CrawlDatums;
import cn.edu.hfut.dmic.webcollector.model.Page;
import cn.edu.hfut.dmic.webcollector.plugin.rocks.BreadthCrawler;

/**
 * 
* @ClassName: ExcelCrawl 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author luoxiao
* @date 2019年10月18日 上午11:04:19 
*
 */
public class ExcelCrawl extends BreadthCrawler{
	
	private List<String> urls;
	
	public ExcelCrawl(String crawlPath, boolean autoParse) {
		super(crawlPath, autoParse);
		//设置线程数
		this.setThreads(1);
		//设置请求cookie
		this.setRequester(new RequestCookie());
	}

	@Override
	public void visit(Page page, CrawlDatums next) {
		Document doc = page.doc();
		
		List<String> urls = new ArrayList<String>();
		//获取页面总数
		Integer count = Integer.valueOf(doc.select("table").get(1).select("form").get(0).select("b").get(0).text());
		if(count > 1000) {
			//取整
			int num = count/1000;
			int relax = count - num * 1000;
			if(relax == 0) {
				for(int i=1;i<=num;i++) {
					urls.add(page.url()+"&pageSize=1000"+"&page="+i);
				}
			}else {
				for(int i=1;i<=num+1;i++) {
					urls.add(page.url()+"&pageSize=1000"+"&page="+i);
				}
			}
		}else {
			urls.add(page.url()+"&pageSize="+count+"&page=1");
		}
		this.setUrls(urls);
	}

	public List<String> getUrls() {
		return urls;
	}

	public void setUrls(List<String> urls) {
		this.urls = urls;
	}

}
