package com.project.app.cache;

public class StartFlag {
	
	/**0:未启动  1：启动*/
	private static Integer wordPriceFlag = 0;
	
	private static Integer wordBaseFlag = 0;
	
	private static Integer plasticFlag = 0; 
	
	private static Integer colorFlag = 0;
	
	private static Integer excelSteelFlag = 0;
	
	private static Integer excelAluminumFlag = 0;
	
	private static Integer excelCopperFlag = 0;
	
	private static Integer excelCellFlag = 0;
	
	private static Integer excelPlasticFlag = 0;
	
	private static Integer qlmFlag = 0;
	
	private static Integer alFlag = 0;
	
	public static void alStart() {
		alFlag = 1;
	}
	
	public static void  alIsStart() {
		if(alFlag == 1) {
			throw new RuntimeException("其他人正在爬取数据!");
		}
	}
	
	public static void alStop() {
		alFlag = 0;
	}
	
	public static void qlmStart() {
		qlmFlag = 1;
	}
	
	public static void  qlmIsStart() {
		if(qlmFlag == 1) {
			throw new RuntimeException("其他人正在爬取数据!");
		}
	}
	
	public static void qlmStop() {
		qlmFlag = 0;
	}
	
	
	public static void excelPlasticStart() {
		excelPlasticFlag = 1;
	}
	
	public static void  excelPlasticIsStart() {
		if(excelPlasticFlag == 1) {
			throw new RuntimeException("其他人正在爬取数据!");
		}
	}
	
	public static void excelPlasticStop() {
		excelPlasticFlag = 0;
	}
	
	public static void wordPriceStart() {
		wordPriceFlag = 1;
	}
	
	public static void  wordPriceIsStart() {
		if(wordPriceFlag == 1) {
			throw new RuntimeException("其他人正在爬取数据!");
		}
	}
	
	public static void wordPriceStop() {
		wordPriceFlag = 0;
	}
	
	public static void wordBaseStart() {
		wordBaseFlag = 1;
	}
	
	public static void  wordBaseIsStart() {
		if(wordBaseFlag == 1) {
			throw new RuntimeException("其他人正在爬取数据!!");
		}
	}
	
	public static void wordBaseStop() {
		wordBaseFlag = 0;
	}
	
	public static void excelSteelStart() {
		excelSteelFlag = 1;
	}
	
	public static void  excelSteelIsStart() {
		if(excelSteelFlag == 1) {
			throw new RuntimeException("其他人正在爬取数据!");
		}
	}
	
	public static void excelSteelStop() {
		excelSteelFlag = 0;
	}
	
	public static void excelAluminumStart() {
		excelAluminumFlag = 1;
	}
	
	public static void  excelAluminumIsStart() {
		if(excelAluminumFlag == 1) {
			throw new RuntimeException("其他人正在爬取数据!");
		}
	}
	
	public static void excelAluminumStop() {
		excelAluminumFlag = 0;
	}
	
	public static void excelCopperStart() {
		excelCopperFlag = 1;
	}
	
	public static void  excelCopperIsStart() {
		if(excelCopperFlag == 1) {
			throw new RuntimeException("其他人正在爬取数据!");
		}
	}
	
	public static void excelCopperStop() {
		excelCopperFlag = 0;
	}
	
	public static void excelCellStart() {
		excelCellFlag = 1;
	}
	
	public static void  excelCellIsStart() {
		if(excelCellFlag == 1) {
			throw new RuntimeException("其他人正在爬取数据!");
		}
	}
	
	public static void excelCellStop() {
		excelCellFlag = 0;
	}
	
	public static void plasticStart() {
		plasticFlag = 1;
	}
	
	public static void  plasticIsStart() {
		if(plasticFlag == 1) {
			throw new RuntimeException("其他人正在爬取数据!");
		}
	}
	
	public static void plasticStop() {
		plasticFlag = 0;
	}
	
	public static void colorStart() {
		colorFlag = 1;
	}
	
	public static void  colorIsStart() {
		if(colorFlag == 1) {
			throw new RuntimeException("其他人正在爬取数据!");
		}
	}
	
	public static void colorStop() {
		colorFlag = 0;
	}
	
}
