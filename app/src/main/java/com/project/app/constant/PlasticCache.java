package com.project.app.constant;

import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;

@Component
public class PlasticCache implements ApplicationRunner{

	@Value("classpath:plastic.json")
	private Resource plastic;
	
	public static Map<String,List<String>> map = new HashMap<String,List<String>>();
	
	@Override
	public void run(ApplicationArguments args) throws Exception {
		String city =  IOUtils.toString(plastic.getInputStream(), Charset.forName("UTF-8"));
		map = JSONObject.parseObject(city, new TypeReference<Map<String, List<String>>>(){});
	}

}
