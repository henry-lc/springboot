package com.project.app.constant;

public class CrawlDeputy {

	public enum CrawlExcelEnum {
		STELL_PRICE_WORD("废钢调价WORD","1"), STELL_BASE_WORD("废钢基地WORD","2"),
		PACSTIC_WORD("塑料WORD","3"),COLOR_WORD("废有色WORD","4"),
		STEEL_EXCEL("废钢(市场调价  钢厂调价excel)","5"),COPPER_EXCEL("废铜(市场调价  钢厂调价excel)","6"),
		ALLALUMINUM_EXCEL("废铝(富宝废旧excel)","7"),CELL_EXCEL("废电池钢(市场调价  钢厂调价excel)","8"),
		QLM_DATA("千里马","9"),PLASTIC_EXCEL("废塑料(富宝废旧excel)","11");
		
		private String name;
		
		private String code;
		
		private CrawlExcelEnum(String name, String code) {
			this.name = name;
			this.code = code;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}
	}
}
