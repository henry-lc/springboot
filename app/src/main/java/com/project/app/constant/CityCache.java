package com.project.app.constant;

import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.project.app.entity.ExcelEntity;

@Component
public class CityCache implements ApplicationRunner{

	@Value("classpath:city.json")
	private Resource crawls;
	
	public static Map<String,List<String>> map = new HashMap<String,List<String>>();
	
	@Override
	public void run(ApplicationArguments args) throws Exception {
		String city =  IOUtils.toString(crawls.getInputStream(), Charset.forName("UTF-8"));
		map = JSONObject.parseObject(city, new TypeReference<Map<String, List<String>>>(){});
	}
	
	public static List<String> getCityByProvince(String distract){
		Map<String,List<String>> cityMap = CityCache.map;
		List<String> province = null;
		for(Entry<String,List<String>> entry : cityMap.entrySet()) {
			String key = entry.getKey();
			if(key.indexOf(distract) > -1) {
				province = entry.getValue();
				break;
			}
		}
		return province;
	}
	
	public static void setExcelEntityProvince(List<ExcelEntity> lists){
		if(lists != null && lists.size()>0) {
			lists.stream().forEach(a->{
				String  distract = a.getDistract();
				a.setProvince("未知");
				for(Entry<String,List<String>> entry : map.entrySet()) {
					String province = entry.getKey();
					List<String> city = entry.getValue();
					if(city.toString().indexOf(distract)>-1) {
						a.setProvince(province);
						break;
					}
				}
			});
		}
	}

}
