package com.project.app.constant;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

import com.project.app.entity.NewsEntity;


public class ImageCache {

	private static ImageCache imageCache = null;
	
	private static ReentrantLock lock = new ReentrantLock();
	
	private List<NewsEntity> recognitionImageLists = new ArrayList<NewsEntity>();
	
	private static void syncInit(){
		lock.lock();
		if(imageCache == null){
			imageCache = new ImageCache();
		}
		lock.unlock();
	}
	
	public static ImageCache getInstance() {
		if(imageCache == null) {
			syncInit();
		}
		return imageCache;
	}

	public  List<NewsEntity> getRecognitionImageLists() {
		return recognitionImageLists;
	}

	public  void setRecognitionImageLists(List<NewsEntity> recognitionImageLists) {
		this.recognitionImageLists = recognitionImageLists;
	}
	
	
	
}
