package com.project.app.constant;

import java.util.HashMap;
import java.util.Map;

public class FilterConstant {
	
	public final static String[] filterSteel = {"大量收","大量回收","大量库存","大量出售","大量求购","高价回收","高价收购",
			"大量现款回收","回收站","材料库","长期现款收","物资收","江阴标亮固废码头","现款收","向威资源有限公司","长期收-购","绍兴鑫晨回收",
			"现-金收卸货付","高价现-金收-购","常州市闽润物资回收","靖江市鑫鑫废旧金属回收","现款收","江门新会百江再生资源采-购价","长期购销","长期收-购"
			,"桐乡炉头国钢码头废旧物资回收"};

	public final static String[] filterColor = {"废铜","电瓶"};
	
	
	public final static String[] filterAlloy = {"H13块料","P91块料","P91刨花","P91刨花","P92刨花","15CrMo块料","15CrMo刨花","P22块料","P22刨花",
			"12CrMo块料","25Cr2Ni4Mo块料","25Cr2Ni4Mo刨花","34CrNi3Mo刨花","42CrMo块料","42CrMo刨花","17CrNiMo6刨花","F11刨花","18CrNiMo刨花",
			"Cr12MoV刨花","Cr12MoV块料","LD块料","LD刨花","3Cr13块料","3Cr13刨花","NK80(Ni3)块料","3Cr258块料","3Cr258刨花","H13挤压模","20Cr2Ni4",
			"42CrMo","锤头","高锰钢","5CrNiMo块料","17CrNiMo6块料","4130块料","Cr13刨花","Ni2刨花","20Cr2Ni4块料","P92块料","34CrNiMo刨花","H13刨花",
			"NK80（Ni3）块料"};
	
	
	public final static String[] filterPlaticExcel = {"HDPE","PP","PVC","HIPS","ABS","EVA","PET","PC",
			"PA","LDPE","EPS","PS","PMMA","低压","高压"};
	
	public final static Map<String,String> map = new HashMap<String, String>();
	
	static {
		map.put("므", "");map.put("元吨", "元/吨");map.put(";","；");
		map.put("口", "");map.put("99废纸之家", "废品");map.put("参入", "掺入");
		map.put("口", "");map.put("（","(");map.put("参假", "掺假");
		map.put("ロ", "");map.put("）",")");map.put("参水", "掺水");
		map.put("모", "");map.put(",","，");map.put("参杂", "掺杂");
		map.put("망", "");map.put(".","。");map.put("渗", "掺");
		map.put("人伪", "人为");map.put("水分", "水份");map.put("~", "-");
		map.put("纸：", "纸");map.put("级：", "级");map.put("箱：", "箱");map.put("板：", "板");
		map.put("边：", "边");map.put("花：", "花");map.put("壹", "一");map.put("贰", "二");
		map.put("叁", "三");map.put("元每吨", "元/吨");map.put("纤业","纸业");
		map.put("및정", "");map.put("및", "");map.put("정", "");
	}
}
