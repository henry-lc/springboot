package com.project.app.cookie;

import java.util.List;

public interface CookieHandle {

	public List<String> getCookieList();
}
