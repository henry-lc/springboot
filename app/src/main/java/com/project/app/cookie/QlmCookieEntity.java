package com.project.app.cookie;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

public class QlmCookieEntity implements CookieHandle{

	private static QlmCookieEntity qlmCookieEntity = null;
	
	private static ReentrantLock lock = new ReentrantLock();
	
	private List<String> cookie = new ArrayList<String>();
	
	private static void syncInit() {
		lock.lock();
		if(qlmCookieEntity == null) {
			qlmCookieEntity = new QlmCookieEntity();
		}
		lock.unlock();
	}
	
	public static QlmCookieEntity getInstance() {
		if(qlmCookieEntity == null) {
			syncInit();
		}
		return qlmCookieEntity;
	}

	public  List<String> getCookie() {
		return cookie;
	}

	public  void setCookie(List<String> cookie) {
		this.cookie = cookie;
	}
	
	public static boolean isEmpty() {
		if(qlmCookieEntity == null || qlmCookieEntity.getCookie() == null || qlmCookieEntity.getCookie().size() <=0) {
			return true;
		}
		return false;
	}

	@Override
	public List<String> getCookieList() {
		return getInstance().getCookie();
	}
}
