package com.project.app.ocr.baidu;

import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;

import com.baidu.aip.ocr.AipOcr;
import com.project.app.ocr.constant.BaiduAuth;
import com.project.app.ocr.handle.OCRHandle;

public class BaiduCommonOcrHandler implements OCRHandle{

	@Override
	public String read(byte[] file) {
		AipOcr client = new AipOcr(BaiduAuth.APP_ID, BaiduAuth.API_KEY, BaiduAuth.SECRET_KEY);
		client.setConnectionTimeoutInMillis(2000);
	    client.setSocketTimeoutInMillis(60000);
	    
	    HashMap<String, String> options = new HashMap<String, String>();
	    options.put("language_type", "CHN_ENG");
	    options.put("detect_direction", "true");
	    options.put("detect_language", "true");
	    options.put("probability", "true");
	    
	    org.json.JSONObject res = client.basicGeneral(file, options);
	       
	    JSONArray  wordResults = (JSONArray) res.get("words_result");
	    StringBuffer sb = new StringBuffer();
	    for(int i=0;i<wordResults.length();i++) {
	    	JSONObject word = (JSONObject) wordResults.get(i);
	    	sb.append(word.get("words").toString());
	    }
		return sb.toString();
	}
	
	
	public String read(String url) {
		AipOcr client = new AipOcr(BaiduAuth.APP_ID, BaiduAuth.API_KEY, BaiduAuth.SECRET_KEY);
		client.setConnectionTimeoutInMillis(2000);
	    client.setSocketTimeoutInMillis(60000);
	    
	    HashMap<String, String> options = new HashMap<String, String>();
	    options.put("detect_direction", "true");
	    options.put("detect_language", "true");
	    options.put("probability", "true");
	    
	    org.json.JSONObject res = client.basicGeneralUrl(url, options);
	       
	    JSONArray  wordResults = (JSONArray) res.get("words_result");
	    StringBuffer sb = new StringBuffer();
	    for(int i=0;i<wordResults.length();i++) {
	    	JSONObject word = (JSONObject) wordResults.get(i);
	    	sb.append(word.get("words").toString());
	    }
		return sb.toString();
	}
}
