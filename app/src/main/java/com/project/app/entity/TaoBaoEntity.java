package com.project.app.entity;

import java.io.Serializable;

public class TaoBaoEntity implements Serializable{

	/** 
	* @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么) 
	*/ 
	private static final long serialVersionUID = 1L;

	private String title;
	
	private String content;
	
	private String urls;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}	

	public String getUrls() {
		return urls;
	}

	public void setUrls(String urls) {
		this.urls = urls;
	}

	@Override
	public String toString() {
		return "TaoBaoEntity [title=" + title + ", content=" + content + ", urls=" + urls + "]";
	}

	
	
	
}
