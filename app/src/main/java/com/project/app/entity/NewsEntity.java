package com.project.app.entity;

import java.io.Serializable;

/**
 * 
* @ClassName: NewsEntity 
* @Description: 新闻实体类
* @author luoxiao
* @date 2019年10月17日 下午2:38:46 
*
 */
public class NewsEntity implements Serializable{

	/** 
	* @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么) 
	*/ 
	private static final long serialVersionUID = 1L;
	
	private String id;

	private String dateTime;
	
	private String title;
	
	private String description;
	
	private String region;

	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	@Override
	public String toString() {
		return "NewsEntity [id=" + id + ", dateTime=" + dateTime + ", title=" + title + ", description=" + description
				+ ", region=" + region + "]";
	}

	

	
}
