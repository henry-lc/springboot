package com.project.app.entity;

import java.io.Serializable;

/**
 * 
* @ClassName: ExcelEntity 
* @Description: 废铁 废铝 废铜  废
* @author luoxiao
* @date 2019年10月18日 上午11:06:31 
*
 */
public class ExcelEntity implements Serializable{

	/** 
	* @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么) 
	*/ 
	private static final long serialVersionUID = 1L;

	private String id;
	
	private String productName;
	
	private String category;
	
	private String distract;
	
	private String price;
	
	private String upDown;
	
	private String date;
	
	private String tax;
	
	private String region;
	
	private String province;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getDistract() {
		return distract;
	}

	public void setDistract(String distract) {
		this.distract = distract;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getUpDown() {
		return upDown;
	}

	public void setUpDown(String upDown) {
		this.upDown = upDown;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
	
	public String getTax() {
		return tax;
	}

	public void setTax(String tax) {
		this.tax = tax;
	}
	
	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}
	
	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	@Override
	public String toString() {
		return "ExcelEntity [productName=" + productName + ", category=" + category + ", distract=" + distract
				+ ", price=" + price + ", upDown=" + upDown + ", date=" + date + ", tax=" + tax + "]";
	}
	
}
