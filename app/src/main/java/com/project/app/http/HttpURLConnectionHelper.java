package com.project.app.http;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpURLConnectionHelper {

	 public static String sendRequest(String urlParam,String requestType) {
		 HttpURLConnection con =  null;
		 BufferedReader buffer = null;
		 StringBuffer resultBuffer = null;
		 try {
			 URL url = new URL(urlParam); 
			 con = (HttpURLConnection) url.openConnection(); 
			 con.setRequestMethod(requestType);  
			 con.setRequestProperty("Content-Type", "application/json;charset=utf-8");  
			 con.setDoOutput(true);
			 con.setUseCaches(false);
			 int responseCode = con.getResponseCode();
			 if(responseCode == HttpURLConnection.HTTP_OK){
				 InputStream inputStream = con.getInputStream();
				 resultBuffer = new StringBuffer();
				 String line = null;
				 buffer = new BufferedReader(new InputStreamReader(inputStream, "utf-8"));
                 while ((line = buffer.readLine()) != null) {
                    resultBuffer.append(line);
                 }
	             return resultBuffer.toString();
			 }
		 }catch(Exception e) {
            e.printStackTrace();
        }
		 return "";
	 } 	
}	
