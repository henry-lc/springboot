package com.project.app.http;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.alibaba.fastjson.JSONObject;
import com.project.app.cookie.CookieHandle;

@Component
public class HttpRequest {
	
	@Autowired
	private RestTemplate restTemplate;
	
	private CookieHandle cookieHandle;
	
	public CookieHandle getCookieHandle() {
		return cookieHandle;
	}

	public void setCookieHandle(CookieHandle cookieHandle) {
		this.cookieHandle = cookieHandle;
	}
	
	public JSONObject postTestForObject(String url,MultiValueMap<String, Object> params) {
		HttpHeaders httpHeader = new HttpHeaders();
		httpHeader.put("Cookie", cookieHandle.getCookieList());
		httpHeader.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
		httpHeader.add("Content-Type", "text/plain;charset=UTF-8");
		HttpEntity<MultiValueMap<String, Object>> httpEntity = new HttpEntity<MultiValueMap<String, Object>>(params, httpHeader);  
		restTemplate.getMessageConverters().add(new MappingJacksonHttpMessageConverter());
		JSONObject jsonObject = restTemplate.exchange(url, HttpMethod.POST, httpEntity, JSONObject.class).getBody();
		return jsonObject;
	}
	
	public JSONObject postJsonForObject(String url,String json) {
		HttpHeaders httpHeader = new HttpHeaders();
		httpHeader.add("Content-Type", "application/json;charset=UTF-8");
		HttpEntity<String> formEntity = new HttpEntity<String>(json, httpHeader);
		JSONObject jsonObject = restTemplate.exchange(url, HttpMethod.POST, formEntity, JSONObject.class).getBody();
		return jsonObject;
	}
	

}
