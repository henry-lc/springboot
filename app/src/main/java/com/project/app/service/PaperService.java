package com.project.app.service;

import java.util.List;

import com.project.app.entity.NewsEntity;

public interface PaperService {

	public  List<NewsEntity>  recognitionCommonImage(List<byte[]> file);
	
	
	public  List<NewsEntity>  rerecognitionSeniorImage(List<byte[]> file);
	
}
