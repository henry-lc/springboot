package com.project.app.service.impl;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.project.app.cache.FinalDataCache;
import com.project.app.cache.StartFlag;
import com.project.app.constant.CrawlCache;
import com.project.app.constant.CrawlDeputy;
import com.project.app.constant.DistractCache;
import com.project.app.constant.FilterConstant;
import com.project.app.entity.NewsEntity;
import com.project.app.service.ColorService;
import com.project.app.util.ForEachUtils;
import com.project.app.webcollect.collector.DataCache;
import com.project.app.webcollect.color.ColorContentCrawl;
import com.project.app.webcollect.color.ColorTitleCrawl;

@Service("colorService")
public class ColorServiceImpl implements ColorService{

	@Override
	public List<NewsEntity> getUpdateColor() {
		StartFlag.colorIsStart();
		StartFlag.colorStart();
		crawlTarget(CrawlDeputy.CrawlExcelEnum.COLOR_WORD.getCode());
		StartFlag.colorStop();
		List<NewsEntity> newsData = filterColor(ColorContentCrawl.results);
		return getUpdateData(newsData,CrawlDeputy.CrawlExcelEnum.COLOR_WORD.getCode());
	}

	@Override
	public List<NewsEntity> getAllColorByCrawl() {
		StartFlag.colorIsStart();
		StartFlag.colorStart();
		crawlTarget(CrawlDeputy.CrawlExcelEnum.COLOR_WORD.getCode());
		StartFlag.colorStop();
		//List<NewsEntity> filters = filterColor(ColorContentCrawl.results);
		List<NewsEntity> results = handleData(ColorContentCrawl.results,CrawlDeputy.CrawlExcelEnum.COLOR_WORD.getCode());
		return results;
	}
	
	@Override
	public List<NewsEntity> getAllColorByCache(){
		List<NewsEntity> lists = FinalDataCache.getInstance().getWordMaps().get(CrawlDeputy.CrawlExcelEnum.COLOR_WORD.getCode());
		if(lists != null && lists.size() >0) {
			return lists;
		}
		return null;
	}
	
	private List<NewsEntity> getUpdateData(List<NewsEntity> lists,String lastDateKey){
		try {
			SimpleDateFormat formatOne = new SimpleDateFormat("yyyy-MM-dd");
			String nowTime = formatOne.format(Calendar.getInstance().getTime());
			String lastDateNodeString = DataCache.getInstance().getLastDate().get(lastDateKey);
			if(lastDateNodeString != null && nowTime.equals(lastDateNodeString)) {
				List<NewsEntity> addSteels = lists.stream().filter(s -> s.getDateTime().compareTo(lastDateNodeString)>0).collect(Collectors.toList());
				DataCache.getInstance().getLastDate().put(lastDateKey, addSteels.get(0).getDateTime());
				return addSteels;
			}
			DataCache.getInstance().getLastDate().put(lastDateKey, lists.get(0).getDateTime());
			return lists;
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
		
	}
	
	private void crawlTarget(String key) {
		JSONObject jsonObject = (JSONObject) CrawlCache.jsonObject.get(key);
		String url = jsonObject.getString("url");
		FinalDataCache finalDataCache = FinalDataCache.getInstance();
		if(finalDataCache.getWordMaps().get(key) != null) {
			finalDataCache.getWordMaps().get(key).clear();
		}
		crawlColorTitle(url);
		crawlColorContent(ColorTitleCrawl.list);
	}
	
	private void crawlColorTitle(String url){
		try {
			ColorTitleCrawl.list.clear();
			ColorTitleCrawl ctc = new ColorTitleCrawl("/ctc"+"/"+System.currentTimeMillis(),false);
			ctc.addSeed(url);
			ctc.start(2);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	private void crawlColorContent(List<String> url){
		try {
			ColorContentCrawl.results.clear();
			ColorContentCrawl ccc = new ColorContentCrawl("/ccc"+"/"+System.currentTimeMillis(),false);
			ccc.addSeed(url);
			ccc.start(2);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	private List<NewsEntity> handleData(List<NewsEntity> lists,String key) {
		List<NewsEntity> result = null;
		if(lists != null) {
			//新增Id
			ForEachUtils.forEach(0, lists, (index,item)->{
				item.setId(String.valueOf(index));
			});
			//分区
			DistractCache.subRegion(lists, "getTitle", "getDescription","setRegion");
			//时间排序
			result = lists.stream().sorted(Comparator.comparing(NewsEntity::getDateTime).reversed()).collect(Collectors.toList());
			FinalDataCache finalDataCache = FinalDataCache.getInstance();
			finalDataCache.getWordMaps().put(key, result);
		}
		return result;
	}
	
	private List<NewsEntity> filterColor(List<NewsEntity> newsEntity) {
		if(newsEntity == null || newsEntity.size() == 0) {
			throw new RuntimeException("数据为空");
		}
		List<NewsEntity> filterSteels = newsEntity.stream().filter(s->{
			String title = s.getTitle();
			String description = s.getDescription();
			String[] filters = FilterConstant.filterSteel;
			Boolean flag = true;
			for(String filter : filters) {
				if(title.indexOf(filter) >-1) {
					flag = false;
					break;
				}
			}
			if(flag) {
				for(String filter : filters) {
					if(description.indexOf(filter) >-1) {
						flag = false;
						break;
					}
				}
			}
			return flag;
		}).collect(Collectors.toList());
		return filterSteels;
	}

}
