package com.project.app.service.impl;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.project.app.cache.FinalDataCache;
import com.project.app.cache.StartFlag;
import com.project.app.constant.CrawlCache;
import com.project.app.constant.CrawlDeputy;
import com.project.app.constant.DistractCache;
import com.project.app.constant.FilterConstant;
import com.project.app.entity.NewsEntity;
import com.project.app.service.SteelService;
import com.project.app.util.ForEachUtils;
import com.project.app.webcollect.collector.DataCache;
import com.project.app.webcollect.steel.SteelContentCrawl;
import com.project.app.webcollect.steel.SteelTitleCrawl;

@Service("steelService")
public class SteelServiceImpl implements SteelService{
		
	@Override
	public List<NewsEntity> getUpateNewPriceByCrawl() {
		StartFlag.wordPriceIsStart();
		StartFlag.wordPriceStart();
		crawlTarget(CrawlDeputy.CrawlExcelEnum.STELL_PRICE_WORD.getCode(),"price");
		StartFlag.wordPriceStop();
		List<NewsEntity> newsData = filterSteel(DataCache.getInstance().getWordMaps().get(CrawlDeputy.CrawlExcelEnum.STELL_PRICE_WORD.getCode()));
		return getUpdateData(newsData,CrawlDeputy.CrawlExcelEnum.STELL_PRICE_WORD.getCode());
	}
	



	@Override
	public List<NewsEntity> getAllNewPriceByCrawl() {
		StartFlag.wordPriceIsStart();
		StartFlag.wordPriceStart();
		crawlTarget(CrawlDeputy.CrawlExcelEnum.STELL_PRICE_WORD.getCode(),"price");
		StartFlag.wordPriceStop();
		List<NewsEntity> filters = filterSteel(DataCache.getInstance().getWordMaps().get(CrawlDeputy.CrawlExcelEnum.STELL_PRICE_WORD.getCode()));
		List<NewsEntity> results = handleData(filters,CrawlDeputy.CrawlExcelEnum.STELL_PRICE_WORD.getCode());
		return  results;
	}


	@Override
	public List<NewsEntity> getUpateNewBaseByCrawl() {
		StartFlag.wordBaseIsStart();
		StartFlag.wordBaseStart();
		crawlTarget(CrawlDeputy.CrawlExcelEnum.STELL_BASE_WORD.getCode(),"base");
		StartFlag.wordBaseStop();
		List<NewsEntity> newsData = filterSteel(DataCache.getInstance().getWordMaps().get(CrawlDeputy.CrawlExcelEnum.STELL_BASE_WORD.getCode()));
		
		return getUpdateData(newsData,CrawlDeputy.CrawlExcelEnum.STELL_BASE_WORD.getCode());
	}


	@Override
	public List<NewsEntity> getAllNewBaseByCrawl() {
		StartFlag.wordBaseIsStart();
		StartFlag.wordBaseStart();
		crawlTarget(CrawlDeputy.CrawlExcelEnum.STELL_BASE_WORD.getCode(),"base");
		StartFlag.wordBaseStop();
		List<NewsEntity> filters = filterSteel(DataCache.getInstance().getWordMaps().get(CrawlDeputy.CrawlExcelEnum.STELL_BASE_WORD.getCode()));
		List<NewsEntity> results = handleData(filters,CrawlDeputy.CrawlExcelEnum.STELL_BASE_WORD.getCode());
		return results;
	}
	
	
	@Override
	public List<NewsEntity> getAllNewPriceByCache() {
		List<NewsEntity> lists = FinalDataCache.getInstance().getWordMaps().get(CrawlDeputy.CrawlExcelEnum.STELL_PRICE_WORD.getCode());
		if(lists != null && lists.size() >0) {
			return lists;
		}
		return null;
	}

	@Override
	public List<NewsEntity> getAllNewBaseByCache() {
		List<NewsEntity> lists = FinalDataCache.getInstance().getWordMaps().get(CrawlDeputy.CrawlExcelEnum.STELL_BASE_WORD.getCode());
		if(lists != null && lists.size() >0) {
			return lists;
		}
		return null;
	}
	
	private void crawlTarget(String key,String productName) {
		JSONObject jsonObject = (JSONObject) CrawlCache.jsonObject.get(key);
		String url = jsonObject.getString("url");
		DataCache dataCache = DataCache.getInstance();
		FinalDataCache finalDataCache = FinalDataCache.getInstance();
		if(dataCache.getWordMaps().get(key) != null) {
			dataCache.getWordMaps().get(key).clear();
		}
		if(finalDataCache.getWordMaps().get(key) != null) {
			finalDataCache.getWordMaps().get(key).clear();
		}
		List<String> urls = crawlTitle(url,productName);
		crawlContent(urls,productName);
	}
	
	
	private List<NewsEntity> getUpdateData(List<NewsEntity> lists,String lastDateKey){
		try {
			SimpleDateFormat formatOne = new SimpleDateFormat("yyyy-MM-dd");
			String nowTime = formatOne.format(Calendar.getInstance().getTime());
			String lastDateNodeString = DataCache.getInstance().getLastDate().get(lastDateKey);
			if(lastDateNodeString != null && nowTime.equals(lastDateNodeString)) {
				List<NewsEntity> addSteels = lists.stream().filter(s -> s.getDateTime().compareTo(lastDateNodeString)>0).collect(Collectors.toList());
				DataCache.getInstance().getLastDate().put(lastDateKey, addSteels.get(0).getDateTime());
				return addSteels;
			}
			DataCache.getInstance().getLastDate().put(lastDateKey, lists.get(0).getDateTime());
			return lists;
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
		
	}
	
	private List<NewsEntity> filterSteel(List<NewsEntity> steels) {
		if(steels == null || steels.size() == 0) {
			throw new RuntimeException("数据为空");
		}
		List<NewsEntity> filterSteels = steels.stream().filter(s->{
			String title = s.getTitle();
			String description = s.getDescription();
			String[] filters = FilterConstant.filterSteel;
			Boolean flag = true;
			for(String filter : filters) {
				if(title.indexOf(filter) >-1) {
					flag = false;
					break;
				}
			}
			if(flag) {
				for(String filter : filters) {
					if(description.indexOf(filter) >-1) {
						flag = false;
						break;
					}
				}
			}
			return flag;
		}).collect(Collectors.toList());
		return filterSteels;
	}

	
	private List<String> crawlTitle(String url,String productName) {
		try {
			SteelTitleCrawl crawl = new SteelTitleCrawl("/word/"+productName+"/"+System.currentTimeMillis(),false);
			crawl.addSeed(url);
			crawl.start(2);
			return crawl.getUrls();
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	private void crawlContent(List<String> urls,String productName) {
		try {
			SteelContentCrawl crawl = new SteelContentCrawl("/wordcontent/"+productName+"/"+System.currentTimeMillis(),false);
			crawl.addSeed(urls);
			crawl.start(2);
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}


	private List<NewsEntity> handleData(List<NewsEntity> lists,String key) {
		List<NewsEntity> result = null;
		if(lists != null) {
			//新增Id
			ForEachUtils.forEach(0, lists, (index,item)->{
				item.setId(String.valueOf(index));
			});
			//分区
			DistractCache.subRegion(lists, "getTitle", "getDescription","setRegion");
			//时间排序
			result = lists.stream().sorted(Comparator.comparing(NewsEntity::getDateTime).reversed()).collect(Collectors.toList());
			FinalDataCache finalDataCache = FinalDataCache.getInstance();
			finalDataCache.getWordMaps().put(key, result);
		}
		return result;
	}

	
}
