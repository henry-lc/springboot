package com.project.app.service;

import java.util.List;

import com.project.app.entity.ExcelEntity;

public interface ExcelService {

	public List<ExcelEntity> getAllSteelByCrawl();
	
	public List<ExcelEntity> getAllSteelByCache();
	
	public List<ExcelEntity> getAllAluminumByCrawl();
	
	public List<ExcelEntity> getAllAluminumByCache();
	
	public List<ExcelEntity> getAllCopperByCrawl();
	
	public List<ExcelEntity> getAllCopperByCache();
	
	public List<ExcelEntity> getAllCellByCrawl();
	
	public List<ExcelEntity> getAllCellByCache();
	
	public List<ExcelEntity> getAllPlasticExcelByCrawl();
	
	public List<ExcelEntity> getAllPlasticByCache();
 }
