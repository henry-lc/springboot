package com.project.app.service.impl;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.project.app.cache.FinalDataCache;
import com.project.app.cache.StartFlag;
import com.project.app.constant.CrawlCache;
import com.project.app.constant.CrawlDeputy;
import com.project.app.constant.DistractCache;
import com.project.app.entity.NewsEntity;
import com.project.app.service.PlasticService;
import com.project.app.util.ForEachUtils;
import com.project.app.webcollect.collector.DataCache;
import com.project.app.webcollect.plastic.PlasticContentCrawl;
import com.project.app.webcollect.plastic.PlasticTitleCrawl;

@Service("plasticService")
public class PlasticServiceImpl implements PlasticService{

	@Override
	public List<NewsEntity> getUpdatePlastic() {
		StartFlag.plasticIsStart();
		//开始标识
		StartFlag.plasticStart();
		crawlTarget(CrawlDeputy.CrawlExcelEnum.PACSTIC_WORD.getCode());
		//结束标识
		StartFlag.plasticStop();		
		return getUpdateData(PlasticContentCrawl.results,CrawlDeputy.CrawlExcelEnum.PACSTIC_WORD.getCode());
	}

	@Override
	public List<NewsEntity> getAllPlasticByCrawl() {
		StartFlag.plasticIsStart();
		//开始标识
		StartFlag.plasticStart();
		//1.爬取塑料标题链接
		crawlTarget(CrawlDeputy.CrawlExcelEnum.PACSTIC_WORD.getCode());
		//结束标识
		StartFlag.plasticStop();	
		List<NewsEntity> results = handleData(PlasticContentCrawl.results,CrawlDeputy.CrawlExcelEnum.PACSTIC_WORD.getCode());
		return results;
	}
	
	@Override
	public List<NewsEntity> getAllPlasticByCache() {
		List<NewsEntity> lists = FinalDataCache.getInstance().getWordMaps().get(CrawlDeputy.CrawlExcelEnum.PACSTIC_WORD.getCode());
		if(lists != null && lists.size() >0) {
			return lists;
		}
		return null;
	}
	
	private List<NewsEntity> getUpdateData(List<NewsEntity> lists,String lastDateKey){
		try {
			SimpleDateFormat formatOne = new SimpleDateFormat("yyyy-MM-dd");
			String nowTime = formatOne.format(Calendar.getInstance().getTime());
			String lastDateNodeString = DataCache.getInstance().getLastDate().get(lastDateKey);
			if(lastDateNodeString != null && nowTime.equals(lastDateNodeString)) {
				List<NewsEntity> addSteels = lists.stream().filter(s -> s.getDateTime().compareTo(lastDateNodeString)>0).collect(Collectors.toList());
				DataCache.getInstance().getLastDate().put(lastDateKey, addSteels.get(0).getDateTime());
				return addSteels;
			}
			DataCache.getInstance().getLastDate().put(lastDateKey, lists.get(0).getDateTime());
			return lists;
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
		
	}
	
	private void crawlTarget(String key) {
		JSONObject jsonObject = (JSONObject) CrawlCache.jsonObject.get(key);
		String url = jsonObject.getString("url");
		FinalDataCache finalDataCache = FinalDataCache.getInstance();
		if(finalDataCache.getWordMaps().get(key) != null) {
			finalDataCache.getWordMaps().get(key).clear();
		}
		crawlPlasticTitle(url);
		crawlPlasticContent(PlasticTitleCrawl.list);
	}
	
	
	private void crawlPlasticTitle(String url){
		try {
			PlasticTitleCrawl.list.clear();
			PlasticTitleCrawl ptc = new PlasticTitleCrawl("/ptc"+"/"+System.currentTimeMillis(),false);
			ptc.addSeed(url);
			ptc.start(2);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	private void crawlPlasticContent(List<String> url){
		try {
			PlasticContentCrawl.results.clear();
			PlasticContentCrawl pcc = new PlasticContentCrawl("/pcc"+"/"+System.currentTimeMillis(),false);
			pcc.addSeed(url);
			pcc.start(2);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	private List<NewsEntity> handleData(List<NewsEntity> lists,String key) {
		List<NewsEntity> result = null;
		if(lists != null) {
			//新增Id
			ForEachUtils.forEach(0, lists, (index,item)->{
				item.setId(String.valueOf(index));
			});
			//分区
			DistractCache.subRegion(lists, "getTitle", "getDescription","setRegion");
			//时间排序
			result = lists.stream().sorted(Comparator.comparing(NewsEntity::getDateTime).reversed()).collect(Collectors.toList());
			FinalDataCache finalDataCache = FinalDataCache.getInstance();
			finalDataCache.getWordMaps().put(key, result);
		}
		return result;
	}

}
