package com.project.app.service;

import java.util.List;

import com.project.app.entity.NewsEntity;

public interface ColorService {

	public List<NewsEntity> getUpdateColor();
	
	public List<NewsEntity> getAllColorByCrawl();
	
	public List<NewsEntity> getAllColorByCache();
}
