package com.project.app.service.impl;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.project.app.cache.FinalDataCache;
import com.project.app.cache.StartFlag;
import com.project.app.config.TbEntity;
import com.project.app.entity.TaoBaoEntity;
import com.project.app.http.HttpRequest;
import com.project.app.service.TaoBaoService;
import com.project.app.util.CopyUtils;
import com.project.app.webcollect.taobao.TaoBaoContentCrawl;
import com.project.app.webcollect.taobao.TaoBaoLinkCrawl;
import com.project.app.webcollect.taobao.TaoBaoTitleCrawl;

@Service("taoBaoService")
public class TaoBaoServiceImpl implements TaoBaoService{

	@Autowired
	private TbEntity tb;
	
	@Autowired
	private HttpRequest httpRequest;
	
	@Override
	public void saveToTarget() {
		StartFlag.alIsStart();
		StartFlag.alStart();
		crawlPage();
		List<String> urls = TaoBaoTitleCrawl.getTaoBaoTitleCrawl(TaoBaoLinkCrawl.pagenum);

		TaoBaoContentCrawl.results.clear();
		TaoBaoContentCrawl tbc = new TaoBaoContentCrawl("/tbc/"+System.currentTimeMillis(),false);
		tbc.addSeed(urls);
		try {
			tbc.start(3);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		List<TaoBaoEntity> results = TaoBaoContentCrawl.results;
		StartFlag.alStop();
		List<TaoBaoEntity> list = FinalDataCache.getInstance().getLasttaoBaoCache();
		if(list != null && list.size()>0) {
			try {
				List<TaoBaoEntity> cacheList = CopyUtils.deepCopy(list);
				List<TaoBaoEntity> copyList = CopyUtils.deepCopy(results);
				FinalDataCache.getInstance().getLasttaoBaoCache().clear();
				FinalDataCache.getInstance().setLasttaoBaoCache(copyList);
				results.removeAll(cacheList);
			} catch (ClassNotFoundException | IOException e) {
				e.printStackTrace();
			}
		}
		if(results != null && results.size()>0) {
			httpRequest.postJsonForObject(tb.getSaveUrl(), JSON.toJSONString(results));
		}
	}

	
	private void crawlPage() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	    Calendar calendar = Calendar.getInstance();
	    calendar.add(Calendar.DAY_OF_MONTH, 10);
		String targetDate = sdf.format(calendar.getTime());
		String url = tb.getUrl() + "&auction_start_from="+targetDate+"&auction_start_to="+targetDate;
		TaoBaoLinkCrawl crawl = new TaoBaoLinkCrawl("/taobao",false);
		crawl.addSeed(url);
		try {
			crawl.start(2);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
