package com.project.app.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.project.app.constant.DistractCache;
import com.project.app.constant.ImageCache;
import com.project.app.entity.NewsEntity;
import com.project.app.ocr.RecognitionImage;
import com.project.app.ocr.baidu.BaiduCommonOcrHandler;
import com.project.app.ocr.baidu.BaiduSeniorOcrHandler;
import com.project.app.ocr.handle.OCRHandle;
import com.project.app.service.PaperService;
import com.project.app.util.ForEachUtils;

@Service("paperService")
public class PaperServiceImpl implements PaperService{

	@Override
	public List<NewsEntity> recognitionCommonImage(List<byte[]> file) {
		RecognitionImage recognitionImage = new RecognitionImage();
		OCRHandle handler = new BaiduCommonOcrHandler();
		recognitionImage.setOcrHandle(handler);
		List<NewsEntity> results = new ArrayList<NewsEntity>();
		NewsEntity news = null;
		for(byte[] data : file) {
			String content = recognitionImage.getContent(data);
			news = new NewsEntity();
			news.setDescription(content);
			results.add(news);
		}
		handleData(results);
		ImageCache imageCache = ImageCache.getInstance();
		return imageCache.getRecognitionImageLists();
	}
	
	private void handleData(List<NewsEntity> lists) {
		if(lists != null) {
			//分区
			DistractCache.subRegion(lists, null, "getDescription","setRegion");
			//时间排序
			ImageCache imageCache = ImageCache.getInstance();
			imageCache.getRecognitionImageLists().addAll(lists);
			//新增Id
			ForEachUtils.forEach(0, imageCache.getRecognitionImageLists(), (index,item)->{
				item.setId(String.valueOf(index));
			});
		}
	}

	@Override
	public List<NewsEntity> rerecognitionSeniorImage(List<byte[]> file) {
		RecognitionImage recognitionImage = new RecognitionImage();
		OCRHandle handler = new BaiduSeniorOcrHandler();
		recognitionImage.setOcrHandle(handler);
		List<NewsEntity> results = new ArrayList<NewsEntity>();
		NewsEntity news = null;
		for(byte[] data : file) {
			String content = recognitionImage.getContent(data);
			news = new NewsEntity();
			news.setDescription(content);
			results.add(news);
		}
		handleData(results);
		ImageCache imageCache = ImageCache.getInstance();
		return imageCache.getRecognitionImageLists();
	}
}
