package com.project.app.controller;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.project.app.constant.ImageCache;
import com.project.app.entity.NewsEntity;
import com.project.app.entity.ReturnType;
import com.project.app.ocr.ImageToByte;
import com.project.app.service.PaperService;
import com.project.app.util.DataGridUtils;
import com.project.app.util.FileUtils;
import com.project.app.util.FreemarkerUtils;
import com.project.app.util.ReturnUtils;
import com.project.app.util.WordUtils;


@RequestMapping(value = "paper/image")
@RestController
public class PaperImageController {

	@Autowired
	private PaperService paperService;

	@RequestMapping(value="/upload")
	public ReturnType<?> uplaod(@RequestParam("file") MultipartFile[] files,Integer type) throws Exception{
		if(files != null && files.length>0) {
			List<byte[]> lists = new ArrayList<byte[]>();
			File file = null;
			for(MultipartFile multiFile : files) {
				 file = FileUtils.multipartFileToFile(multiFile);
				 byte[] data = ImageToByte.image2byte(file);
				 lists.add(data);
			}
			if(type == 1) {
				paperService.recognitionCommonImage(lists);
			}else{
				paperService.rerecognitionSeniorImage(lists);
			}
		}
		return ReturnUtils.success("图像识别成功！");
	}

	@RequestMapping(value="/clearImageCache")
	public ReturnType<?> clearImageCache(){
		ImageCache imageCache = ImageCache.getInstance();
		imageCache.getRecognitionImageLists().clear();
		return ReturnUtils.success("缓存清除成功!");
	}

	@RequestMapping(value = "/deleteById")
	public ReturnType<?> deleteById(String id){
		if(StringUtils.isBlank(id)) {
			ReturnUtils.fail("Id不能为空!");
		}
		String ids[] = id.split(",");
		ImageCache imageCache = ImageCache.getInstance();
		List<NewsEntity> newsList = imageCache.getRecognitionImageLists();
		Map<String,NewsEntity> map = newsList.stream().collect(Collectors.toMap(NewsEntity::getId, p->p));
		for(String key : ids) {
			NewsEntity target = map.get(key);
			if(target != null) {
				newsList.remove(target);
			}
		}
		return ReturnUtils.success("删除成功!");
	}

	@RequestMapping(value = "/load")
	public  ReturnType<?> load(){
		ImageCache imageCache = ImageCache.getInstance();
		List<NewsEntity> newsList = imageCache.getRecognitionImageLists();
		List<NewsEntity> sortLists = newsList.stream().sorted(Comparator.comparing(NewsEntity::getRegion)).collect(Collectors.toList());
		return ReturnUtils.success(DataGridUtils.toDataGrid(sortLists), "查询成功!");
	}

	@RequestMapping(value = "/exportPaper")
	public ResponseEntity<byte[]> exportBase(String id) throws UnsupportedEncodingException{
		ImageCache imageCache = ImageCache.getInstance();
		List<NewsEntity> lists = imageCache.getRecognitionImageLists();
		String[] ids = id.split(",");
		List<NewsEntity> filter = lists.stream().filter(s->Arrays.asList(ids).contains(s.getId())).collect(Collectors.toList());
		filter.forEach(a -> {
			String  content = WordUtils.concatPaperWordFontColor(a.getDescription());
			a.setDescription(content);
		});
		Map<String,List<NewsEntity>> map = filter.stream().collect(Collectors.groupingBy(NewsEntity::getRegion));
		Map<String,Object> result = new HashMap<String,Object>();
		result.put("map", map);
		String filename = new String(("废纸"+System.currentTimeMillis()+".doc").getBytes("UTF-8"),"iso-8859-1");
		String path = FreemarkerUtils.createDoc(result, "paperTemplate.ftl", "D:/template/废纸", filename);
		return FileUtils.download(path);
	}
}
