package com.project.app.controller;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.app.cache.StartFlag;
import com.project.app.entity.NewsEntity;
import com.project.app.entity.ReturnType;
import com.project.app.service.PlasticService;
import com.project.app.util.DataGridUtils;
import com.project.app.util.FileUtils;
import com.project.app.util.FreemarkerUtils;
import com.project.app.util.ReturnUtils;
import com.project.app.util.WordUtils;

@RequestMapping(value="plastic/word")
@RestController
public class PlasticWordController {

	@Autowired
	private PlasticService plasticService;
	
	@RequestMapping(value = "/getPlasticList")
	public ReturnType<?> getPlasticList(){
		try {
			List<NewsEntity> lists = plasticService.getAllPlasticByCache();		
			return ReturnUtils.success(DataGridUtils.toDataGrid(lists), "查询成功!");
		}catch(Exception e) {
			return ReturnUtils.fail(e.getMessage());
		}
	} 
	
	@RequestMapping(value = "/getPlasticCrawl")
	public ReturnType<?> getPlasticCrawl(){
		try {
			List<NewsEntity> lists = plasticService.getAllPlasticByCrawl();
			return ReturnUtils.success(DataGridUtils.toDataGrid(lists), "查询成功!");
		}catch(Exception e) {
			return ReturnUtils.fail(e.getMessage());
		}
	}
	
	@RequestMapping(value="/savePlastic")
	public ReturnType<?> savePlastic(NewsEntity newsEntity){
		try {
			if(newsEntity != null && newsEntity.getId() != null) {
				List<NewsEntity> lists = plasticService.getAllPlasticByCache();		
				NewsEntity target = lists.stream().filter(s->s.getId().equals(newsEntity.getId())).collect(Collectors.toList()).get(0);
				target.setRegion(newsEntity.getRegion());
				return ReturnUtils.success("修改成功!");
			}
			throw new RuntimeException("修改对象为空!");
		}catch(Exception e) {
			return ReturnUtils.fail(e.getMessage());
		}
	}
	
	@RequestMapping(value = "/exportPlastic")
	public ResponseEntity<byte[]> exportPlastic(String id) throws UnsupportedEncodingException{
		List<NewsEntity> lists = plasticService.getAllPlasticByCache();
		String[] ids = id.split(",");
		List<NewsEntity> filter = lists.stream().filter(s->Arrays.asList(ids).contains(s.getId())).collect(Collectors.toList());
		filter.forEach(a -> {
			String  content = WordUtils.concatOtherWordFontColor(a.getDescription());
			a.setDescription(content);
		});
		Map<String,List<NewsEntity>> map = filter.stream().collect(Collectors.groupingBy(NewsEntity::getRegion));
		Map<String,Object> result = new HashMap<String,Object>();
		result.put("map", map);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		result.put("nowDate", sdf.format(Calendar.getInstance().getTime()));
		String filename = new String(("塑料"+System.currentTimeMillis()+".doc").getBytes("UTF-8"),"iso-8859-1");
		String path = FreemarkerUtils.createDoc(result, "wordTemplate.ftl", "D:/template/塑料", filename);
		return FileUtils.download(path);
	}
	
	@RequestMapping(value = "/clearPlasticCrawlCache")
	public ReturnType<?> clearPlasticCrawlCache(){
		try {
			StartFlag.plasticStop();
			return ReturnUtils.success("爬虫标识缓存清除成功!");
		}catch(Exception e) {
			return ReturnUtils.fail(e.getMessage());
		}
	}
}
