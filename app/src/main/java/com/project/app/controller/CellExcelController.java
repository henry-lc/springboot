package com.project.app.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.app.cache.StartFlag;
import com.project.app.constant.CityCache;
import com.project.app.constant.DistractCache;
import com.project.app.entity.ExcelEntity;
import com.project.app.entity.ReturnType;
import com.project.app.service.ExcelService;
import com.project.app.util.CopyUtils;
import com.project.app.util.DataGridUtils;
import com.project.app.util.FileUtils;
import com.project.app.util.ForEachUtils;
import com.project.app.util.FreemarkerUtils;
import com.project.app.util.ReturnUtils;

@RequestMapping(value = "cell/excel")
@RestController
public class CellExcelController {

	@Autowired
	private ExcelService excelService;
	
	@RequestMapping(value = "cellExcelListByCache")
	public ReturnType<?> cellExcelListByCache(String distract,String region,Integer pageIndex,Integer pageSize){
		try {
			List<ExcelEntity> excelEntitys = excelService.getAllCellByCache();
			if(excelEntitys == null || excelEntitys.size() <= 0) {
				throw new RuntimeException("缓存数据不存在!");
			}
			if(StringUtils.isNotBlank(distract)) {
				List<String> province = CityCache.getCityByProvince(distract);
				if(province != null) {
					excelEntitys = excelEntitys.stream().filter(s->s.getProvince().indexOf(distract)>-1).collect(Collectors.toList());
				}else {
					excelEntitys = excelEntitys.stream().filter(s->s.getDistract().indexOf(distract)>-1).collect(Collectors.toList());
				}	
			}
			if(StringUtils.isNotBlank(region)) {
				excelEntitys = excelEntitys.stream().filter(s->s.getRegion().indexOf(region)>-1).collect(Collectors.toList());
			}
			Integer total = excelEntitys.size();
			if(pageIndex != null && pageSize != null) {
				Integer maxCount = pageIndex*pageSize;
				if(maxCount>=excelEntitys.size()) {
					excelEntitys = excelEntitys.subList((pageIndex-1)*pageSize, excelEntitys.size());
				}else {
					excelEntitys = excelEntitys.subList((pageIndex-1)*pageSize, maxCount);
				}	
			}
			return ReturnUtils.success(DataGridUtils.toDataGrid(excelEntitys,total), "查询成功!");
		}catch(Exception e) {
			return ReturnUtils.fail(e.getMessage());
		}
	}
	
	@RequestMapping(value = "cellExcelListByCrawl")
	public ReturnType<?> cellExcelListByCrawl(Integer pageIndex,Integer pageSize){
		try {
			List<ExcelEntity> excelEntitys = excelService.getAllCellByCrawl();
			Integer total = excelEntitys.size();
			if(pageIndex != null && pageSize != null) {
				excelEntitys = excelEntitys.subList((pageIndex-1)*pageSize, pageIndex*pageSize);
			}
			return ReturnUtils.success(DataGridUtils.toDataGrid(excelEntitys,total), "查询成功!");
		}catch(Exception e) {
			StartFlag.excelCellStop();
			return ReturnUtils.fail(e.getMessage());
		}
	}
	
	@RequestMapping(value = "saveCell")
	public ReturnType<?> saveCell(ExcelEntity excelEntity){
		try {
			if(excelEntity != null && excelEntity.getId() != null) {
				List<ExcelEntity> lists = excelService.getAllCellByCache();
				ExcelEntity target = lists.stream().filter(s->s.getId().equals(excelEntity.getId())).collect(Collectors.toList()).get(0);
				target.setRegion(excelEntity.getRegion());
				return ReturnUtils.success("修改成功!");
			}
			throw new RuntimeException("修改对象为空!");
		}catch(Exception e) {
			return ReturnUtils.fail(e.getMessage());
		}	
	}
	
	@RequestMapping(value = "/clearCellCrawlCache")
	public ReturnType<?> clearCellCrawlCache(){
		try {
			StartFlag.excelCellStop();
			return ReturnUtils.success("爬虫标识缓存清除成功!");
		}catch(Exception e) {
			return ReturnUtils.fail(e.getMessage());
		}
	}
	
	@RequestMapping(value = "/exchange")
	public ReturnType<?> exchange(String id){
		try {
			List<ExcelEntity> lists = excelService.getAllCellByCache();
			String[] ids = id.split(",");
			ForEachUtils.forEach(0, lists, (index,item)->{
				if(Arrays.asList(ids).contains(item.getId())) {
					String distract = item.getDistract();
					String productName = item.getProductName();
					item.setDistract(productName);
					item.setProductName(distract);
					String region = DistractCache.getRegion(item, null, "getDistract");
					item.setRegion(region);
				}
			});
			return ReturnUtils.success(DataGridUtils.toDataGrid(lists,lists.size()),"互换成功!");
		}catch(Exception e) {
			return ReturnUtils.fail(e.getMessage());
		}
	}
	
	@RequestMapping(value = "/exportCellExcel")
	public ResponseEntity<byte[]> exportBase(String id) throws UnsupportedEncodingException{
		List<ExcelEntity> lists = excelService.getAllCellByCache();
		String[] ids = id.split(",");
		List<ExcelEntity> filter = lists.stream().filter(s->Arrays.asList(ids).contains(s.getId())).collect(Collectors.toList());
		SimpleDateFormat oldSdf = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat newSdf = new SimpleDateFormat("MM月dd日");
		List<ExcelEntity> exportList = null;
		try {
			exportList = CopyUtils.deepCopy(filter);
		} catch (ClassNotFoundException | IOException e) {
			e.printStackTrace();
		}
		ForEachUtils.forEach(0, exportList, (index,item)->{
			try {
				Date date = oldSdf.parse(item.getDate());
				String now = newSdf.format(date);
				item.setDate(now);
			} catch (ParseException e1) {
				e1.printStackTrace();
			} 
		});
		Map<String,List<ExcelEntity>> map = exportList.stream().sorted(Comparator.comparing(ExcelEntity::getProvince)).collect(Collectors.groupingBy(ExcelEntity::getRegion));
		Map<String,Object> result = new HashMap<String,Object>();
		result.put("map", map);
		String filename = new String(("废电池"+System.currentTimeMillis()+".xls").getBytes("UTF-8"),"iso-8859-1");
		String path = FreemarkerUtils.createDoc(result, "excelTempalte.ftl", "D:/template/excel/电池", filename);
		return FileUtils.download(path);
	}
}
