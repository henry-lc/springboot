package com.project.app.util;

import java.util.List;

import com.project.app.entity.DataGrid;

public class DataGridUtils {

	public static <T> DataGrid<T> toDataGrid(List<T> lists){
		if(lists != null && lists.size() >0) {
			return new DataGrid<T>(lists.size(), lists);
		}
		return null;
	}
	
	public static <T> DataGrid<T> toDataGrid(List<T> lists,Integer total){
		if(lists != null && lists.size() >0) {
			return new DataGrid<T>(total, lists);
		}
		return null;
	}
		
}
