package com.project.app.util;

import com.project.app.entity.ReturnType;

public class ReturnUtils {
	
	public static <T> ReturnType<T> success(String msg){
		return success(null,msg);
	}

	public static <T> ReturnType<T> success(T t,String msg){
		ReturnType<T> returnType = new ReturnType<T>();
		returnType.setCode("200");
		returnType.setMsg(msg);
		returnType.setT(t);
		return returnType;
	}
	
	public static <T> ReturnType<T> fail(String msg){
		return fail(null,msg);
	}
	
	public static <T> ReturnType<T> fail(T t,String msg){
		ReturnType<T> returnType = new ReturnType<T>();
		returnType.setCode("500");
		returnType.setMsg(msg);
		returnType.setT(t);
		return returnType;
	}
	
}
