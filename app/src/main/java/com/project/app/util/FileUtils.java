package com.project.app.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

public class FileUtils {

	@SuppressWarnings("resource")
	public static ResponseEntity<byte[]> download(String path){
		try {
			File file = new File(path);
			byte[] body = null;
		    InputStream is = new FileInputStream(file);
		    body = new byte[is.available()];
		    is.read(body);
		    HttpHeaders headers = new HttpHeaders();
		    headers.add("Content-Disposition", "attchement;filename=" + file.getName());
		    HttpStatus statusCode = HttpStatus.OK;
		    return new ResponseEntity<byte[]>(body, headers, statusCode);
		}catch(Exception e) {
			throw new RuntimeException(e.getMessage());
		}
	}
	
	@SuppressWarnings("unlikely-arg-type")
	public static File multipartFileToFile(@RequestParam MultipartFile file ) throws Exception {

	    File toFile = null;
	    if(file.equals("")||file.getSize()<=0){
	        file = null;
	    }else {
            InputStream ins = null;
            ins = file.getInputStream();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
            String date = sdf.format(Calendar.getInstance().getTime()); 
            File dirFile = new File("d:\\upload\\"+date+"\\");
            if(!dirFile.exists()) {
            	dirFile.mkdirs();
            }
            toFile = new File("d:\\upload\\"+date+"\\"+file.getOriginalFilename());
            inputStreamToFile(ins, toFile);
            ins.close();
	    }
	    return toFile;
	}
	
	public static void inputStreamToFile(InputStream ins, File file) {
	    try {
	        OutputStream os = new FileOutputStream(file);
	        int bytesRead = 0;
	        byte[] buffer = new byte[8192];
	        while ((bytesRead = ins.read(buffer, 0, 8192)) != -1) {
	            os.write(buffer, 0, bytesRead);
	        }
	        os.close();
	        ins.close();
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	}
}
