package com.project.app.util;

public class WordUtils {

	private final static String[] PAPER_BLUE = {"纸业","打包站","环宇","造纸厂","纸厂","纸品"};
	
	private final static String[] OTHER_BLUE = {"北京","天津","上海","重庆","河北","山西","辽宁","吉林","黑龙江","江苏",
			"浙江","安徽","福建","江西","山东","河南","湖北","湖南","广东","海南","四川","贵州","云南","陕西","甘肃","青海","台湾","内蒙古","广西","西藏","宁夏","新疆","香港","澳门"};
	
	private final static String[] RED = {"上调","上涨","涨价","涨"};
	
	private final static String[] GREEN = {"下调","下跌","降价","降","跌"};
	
	private final static String[] YELLOW = {"停收"};
	
	private final static String BLUE_PREFIX = "<w:r><w:rPr><w:rFonts w:ascii=\"微软雅黑\" w:fareast=\"微软雅黑\" w:h-ansi=\"微软雅黑\" w:cs=\"微软雅黑\" w:hint=\"fareast\"/>"
			+" <wx:font wx:val=\"微软雅黑\"/><w:b/><w:b-cs/><w:color w:val=\"0000FF\"/><w:sz w:val=\"28\"/><w:sz-cs w:val=\"28\"/></w:rPr><w:t>";

	private final static String RED_PREFIX = "<w:r><w:rPr><w:rFonts w:ascii=\"微软雅黑\" w:fareast=\"微软雅黑\" w:h-ansi=\"微软雅黑\" w:cs=\"微软雅黑\" w:hint=\"fareast\"/>"
			+"<wx:font wx:val=\"微软雅黑\"/><w:b/><w:b-cs/><w:color w:val=\"FF0000\"/><w:sz w:val=\"28\"/><w:sz-cs w:val=\"28\"/></w:rPr><w:t>";
	
	private final static String GREEN_PREFIX = "<w:r><w:rPr><w:rFonts w:ascii=\"微软雅黑\" w:fareast=\"微软雅黑\" w:h-ansi=\"微软雅黑\" w:cs=\"微软雅黑\" w:hint=\"fareast\"/>"
			+"<wx:font wx:val=\"微软雅黑\"/><w:b/><w:b-cs/><w:color w:val=\"008000\"/><w:sz w:val=\"28\"/> <w:sz-cs w:val=\"28\"/></w:rPr><w:t>";
	
	private final static String YELLOW_PREFIX = "<w:r wsp:rsidRPr=\"00B74D24\"><w:rPr><w:rFonts w:ascii=\"微软雅黑\" w:fareast=\"微软雅黑\" w:h-ansi=\"微软雅黑\" w:cs=\"微软雅黑\" w:hint=\"fareast\"/>"
			+"<wx:font wx:val=\"微软雅黑\"/><w:b/><w:sz w:val=\"28\"/><w:sz-cs w:val=\"28\"/><w:highlight w:val=\"yellow\"/></w:rPr><w:t>";
	
	private final static String COMMON_PREFIX = "<w:r><w:rPr><w:rFonts w:ascii=\"微软雅黑\" w:fareast=\"微软雅黑\" w:h-ansi=\"微软雅黑\" w:cs=\"微软雅黑\" w:hint=\"fareast\"/>"
			+"<wx:font wx:val=\"微软雅黑\"/><w:sz w:val=\"28\"/><w:sz-cs w:val=\"28\"/></w:rPr><w:t>";
	
	private final static String COMMON_SUBFIX= "</w:t></w:r>";
		
	
	public static String concatOtherWordFontColor(String content) {
		content = COMMON_PREFIX+content+COMMON_SUBFIX;  //拼接通用格式
		return addOtherStyle(content);
	}
	
	public static String concatSteelWordFontColor(String content) {
		content = COMMON_PREFIX+content+COMMON_SUBFIX;  //拼接通用格式
		return addSteelStyle(content);
	}


	public static String concatPaperWordFontColor(String content) {
		content = COMMON_PREFIX+content+COMMON_SUBFIX;  //拼接通用格式
		return addPaperStyle(content);
	}
	
	private static String addPaperStyle(String content) {
		if(content != null) {
			for(String blue : PAPER_BLUE) {
				if(content.indexOf(blue)>-1) {
					String replaceBlue = COMMON_SUBFIX+BLUE_PREFIX+blue+COMMON_SUBFIX+COMMON_PREFIX;
					content = content.replaceAll(blue, replaceBlue);
				}
			}
			for(String red :RED) {
				if(content.indexOf(red)>-1) {
					String replaceRed = COMMON_SUBFIX+RED_PREFIX+red+COMMON_SUBFIX+COMMON_PREFIX;
					content = content.replaceAll(red, replaceRed);
				}
			}
			for(String green :GREEN) {
				if(content.indexOf(green)>-1) {
					String replaceGreen = COMMON_SUBFIX+GREEN_PREFIX+green+COMMON_SUBFIX+COMMON_PREFIX;
					content = content.replaceAll(green, replaceGreen);
				}
			}
			for(String yellow :YELLOW) {
				if(content.indexOf(yellow)>-1) {
					String replaceYellow = COMMON_SUBFIX+YELLOW_PREFIX+yellow+COMMON_SUBFIX+COMMON_PREFIX;
					content = content.replaceAll(yellow, replaceYellow);
				}
			}
		}
		return content;
	}
	
	private static String addOtherStyle(String content) {
		if(content != null) {
			for(String red :RED) {
				if(content.indexOf(red)>-1) {
					String replaceRed = COMMON_SUBFIX+RED_PREFIX+red+COMMON_SUBFIX+COMMON_PREFIX;
					content = content.replaceAll(red, replaceRed);
				}
			}
			for(String green :GREEN) {
				if(content.indexOf(green)>-1) {
					String replaceGreen = COMMON_SUBFIX+GREEN_PREFIX+green+COMMON_SUBFIX+COMMON_PREFIX;
					content = content.replaceAll(green, replaceGreen);
				}
			}
			for(String yellow :YELLOW) {
				if(content.indexOf(yellow)>-1) {
					String replaceYellow = COMMON_SUBFIX+YELLOW_PREFIX+yellow+COMMON_SUBFIX+COMMON_PREFIX;
					content = content.replaceAll(yellow, replaceYellow);
				}
			}
		}
		return content;
	}
	
	private static String addSteelStyle(String content) {
		if(content != null) {
			for(String blue : OTHER_BLUE) {
				if(content.indexOf(blue)>-1) {
					String replaceBlue = COMMON_SUBFIX+BLUE_PREFIX+blue+COMMON_SUBFIX+COMMON_PREFIX;
					content = content.replaceAll(blue, replaceBlue);
				}
			}
			for(String red :RED) {
				if(content.indexOf(red)>-1) {
					String replaceRed = COMMON_SUBFIX+RED_PREFIX+red+COMMON_SUBFIX+COMMON_PREFIX;
					content = content.replaceAll(red, replaceRed);
				}
			}
			for(String green :GREEN) {
				if(content.indexOf(green)>-1) {
					String replaceGreen = COMMON_SUBFIX+GREEN_PREFIX+green+COMMON_SUBFIX+COMMON_PREFIX;
					content = content.replaceAll(green, replaceGreen);
				}
			}
			for(String yellow :YELLOW) {
				if(content.indexOf(yellow)>-1) {
					String replaceYellow = COMMON_SUBFIX+YELLOW_PREFIX+yellow+COMMON_SUBFIX+COMMON_PREFIX;
					content = content.replaceAll(yellow, replaceYellow);
				}
			}
		}
		return content;
	}
}
