package com.project.app.util;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import com.project.app.config.ChromeDriverConfig;

public class WebUtils {

	public static Document getHtmlDocument(String url) {
//		ChromeOptions chromeOption = new ChromeOptions();
//		chromeOption.addArguments("--headless");
		ChromeDriverConfig chrome = (ChromeDriverConfig) SpringUtils.getBean(ChromeDriverConfig.class);
		System.setProperty("webdriver.chrome.driver", chrome.getPosition());
		WebDriver webDriver = new ChromeDriver();
		webDriver.get(url); 
		WebElement webElement = webDriver.findElement(By.xpath("/html"));//获取页面全部信息
		String htmlText=webElement.getAttribute("outerHTML");
		Document doc =Jsoup.parse(htmlText);
		webDriver.close();//必须关闭资源
		return doc;
	}
}
