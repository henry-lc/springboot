package com.project.app.util;

public class SpaceUtils {

	public static String removeSpace(String target) {
		StringBuilder bd = new StringBuilder();
		char[] chars = target.toCharArray();
		for (int i = 0; i < chars.length; i++) {
			if ((chars[i] >= 19968 && chars[i] <= 40869) || (chars[i] >= 97 && chars[i] <= 122)
					|| (chars[i] >= 65 && chars[i] <= 90) ||(chars[i]>=48&&chars[i]<=57) || chars[i] == '-' 
					|| chars[i] == ':' || chars[i] == '/') {
				String valueOf = String.valueOf(chars[i]);
				bd.append(valueOf);
			}
		}
		return bd.toString();
	}
}
