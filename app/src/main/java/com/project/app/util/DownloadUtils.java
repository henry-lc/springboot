package com.project.app.util;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;


public class DownloadUtils {

	/**
	 * 
	* @Title: downloadHtmlToLocal 
	* @Description: 下载网页到本地
	* @param     设定文件 
	* @return void    返回类型 
	* @throws
	 */
	public static String downloadHtmlToLocal(String targetUrl,String path){
		
		FileOutputStream fos = null;
		
		InputStream inputStream = null;
		
		try {
			URL url = new URL(targetUrl);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();  
			//设置超时时间
			conn.setConnectTimeout(10 * 1000);  
			// 防止屏蔽程序抓取而返回403错误  
	        conn.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0; Windows NT; DigExt)");  
	        // 得到输入流  
	        inputStream = conn.getInputStream();  
	        // 获取自己数组  
	        byte[] getData = readInputStream(inputStream); 
	        // 文件保存位置  
	        File saveDir = new File(path);
	        if (!saveDir.exists()) {  
	            saveDir.mkdirs();  
	        }  
	        //获取文件名
	        String [] urlSplit = targetUrl.split("/");
	        
	        String fileName = urlSplit[urlSplit.length-1];
	        
	        File file = new File(saveDir + File.separator + fileName);
	        
	        fos = new FileOutputStream(file); 
	        
	        fos.write(getData);  
	       
	        // System.out.println("info:"+url+" download success");  
	        return saveDir + File.separator + fileName+".html";
	        
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			try {
				 if (fos != null) {  
			            fos.close();  
		        }  
		        if (inputStream != null) {  
		            inputStream.close();  
		        }  
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return "";
	}
	
	/** 
	 * 从输入流中获取字节数组 
	 *  
	 * @param inputStream 
	 * @return 
	 * @throws IOException 
	 */  
	private static byte[] readInputStream(InputStream inputStream) throws IOException {  
	    byte[] buffer = new byte[1024];  
	    int len = 0;  
	    ByteArrayOutputStream bos = new ByteArrayOutputStream();  
	    while ((len = inputStream.read(buffer)) != -1) {  
	        bos.write(buffer, 0, len);  
	    }  
	    bos.close();  
	    return bos.toByteArray();  
	}  
	
	/*** 
     * 下载图片 
     *  
     * @param listImgSrc 
     */  
    public static void DownloadPics(List<Map<String, String>> imgSrcs) {  
        try {  
            for (Map<String, String> img : imgSrcs) {  
                URL uri = new URL(img.get("url")+"");  
                InputStream in = uri.openStream();  
                File file = new File(img.get("path")+"");     //文件路径（路径+文件名）  
		          if (!file.exists()) {   //文件不存在则创建文件，先创建目录  
		              File dir = new File(file.getParent());  
		              if(!dir.exists())
		                 dir.mkdirs();  
		              file.createNewFile();  
		          }  
                FileOutputStream fo = new FileOutputStream(file);  
                byte[] buf = new byte[1024];  
                int length = 0;  
                System.out.println("开始下载:" + img.get("url")+"");  
                while ((length = in.read(buf, 0, buf.length)) != -1) {  
                    fo.write(buf, 0, length);  
                }  
                in.close();  
                fo.close();  
                System.out.println(img.get("url") + "下载完成");  
            }  
        } catch (Exception e) {  
        	e.printStackTrace();
            System.out.println("下载失败");  
        }  
    } 
    
	/** 
	   * 传入文件名以及字符串, 将字符串信息保存到文件中 
	   *  
	   * @param strFilename 
	   * @param strBuffer 
	   * @author guorui
	   */  
	@SuppressWarnings({ "resource", "unused" })
	public static void TextToFile(String strFilename, String strBuffer)  
	  {  
		  
		  byte[] sourceByte = strBuffer.getBytes();  
		  if(null != sourceByte){  
		      try {  
		          File file = new File(strFilename);     //文件路径（路径+文件名）  
		          if (!file.exists()) {   //文件不存在则创建文件，先创建目录  
		              File dir = new File(file.getParent());  
		              if(!dir.exists())
		                 dir.mkdirs();  
		              file.createNewFile();  
		          }  
		          FileOutputStream outStream = new FileOutputStream(file);    //文件输出流用于将数据写入文件  
		          BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(  
		                  new FileOutputStream(file), "gb2312"));  
		          writer.write(strBuffer);  
		          writer.close(); ;
		      } catch (Exception e) {  
		          e.printStackTrace();  
		      }  
		  }  
	  }  
}
