<?xml version="1.0"?>
<?mso-application progid="Excel.Sheet"?>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:dt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:html="http://www.w3.org/TR/REC-html40">
 <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
  <Created>2015-06-05T18:19:34Z</Created>
  <LastSaved>2019-10-22T07:44:09Z</LastSaved>
  <Version>16.00</Version>
 </DocumentProperties>
 <CustomDocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
  <WorkbookGuid dt:dt="string">d07eda24-c64f-4639-8418-c9c32f5d885f</WorkbookGuid>
 </CustomDocumentProperties>
 <OfficeDocumentSettings xmlns="urn:schemas-microsoft-com:office:office">
  <AllowPNG/>
  <RemovePersonalInformation/>
 </OfficeDocumentSettings>
 <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
  <WindowHeight>12645</WindowHeight>
  <WindowWidth>22260</WindowWidth>
  <WindowTopX>0</WindowTopX>
  <WindowTopY>0</WindowTopY>
  <ProtectStructure>False</ProtectStructure>
  <ProtectWindows>False</ProtectWindows>
 </ExcelWorkbook>
 <Styles>
  <Style ss:ID="Default" ss:Name="Normal">
   <Alignment ss:Vertical="Bottom"/>
   <Borders/>
   <Font ss:FontName="等线" x:CharSet="134" ss:Size="11" ss:Color="#000000"/>
   <Interior/>
   <NumberFormat/>
   <Protection/>
  </Style>
  <Style ss:ID="s16">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="微软雅黑" x:CharSet="134" x:Family="Swiss" ss:Size="22"
    ss:Color="#FFFFFF" ss:Bold="1"/>
   <Interior ss:Color="#666699" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s17">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="微软雅黑" x:CharSet="134" x:Family="Swiss" ss:Size="20"
    ss:Color="#FFFFFF" ss:Bold="1"/>
   <Interior ss:Color="#666699" ss:Pattern="Solid"/>
   <NumberFormat/>
  </Style>
  <Style ss:ID="s18">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="微软雅黑" x:CharSet="134" x:Family="Swiss" ss:Size="20"
    ss:Color="#000000"/>
   <Interior/>
  </Style>
  <Style ss:ID="s65">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="微软雅黑" x:CharSet="134" x:Family="Swiss" ss:Size="28"
    ss:Color="#000000" ss:Bold="1"/>
   <Interior/>
  </Style>
  <Style ss:ID="s76">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="微软雅黑" x:CharSet="134" x:Family="Swiss" ss:Size="20"
    ss:Color="#008000" ss:Bold="1"/>
   <Interior/>
  </Style>
  <Style ss:ID="s77">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="微软雅黑" x:CharSet="134" x:Family="Swiss" ss:Size="20"
    ss:Color="#FF0000" ss:Bold="1"/>
   <Interior/>
  </Style>
 </Styles>
 <Worksheet ss:Name="Sheet1">
	  <Table ss:ExpandedColumnCount="7" ss:ExpandedRowCount="2000" x:FullColumns="1"
	   x:FullRows="1" ss:DefaultColumnWidth="54" ss:DefaultRowHeight="14.25">
	   <Column ss:AutoFitWidth="0" ss:Width="69.75"/>
	   <Column ss:AutoFitWidth="0" ss:Width="117"/>
	   <Column ss:AutoFitWidth="0" ss:Width="139.5"/>
	   <Column ss:AutoFitWidth="0" ss:Width="58.5"/>
	   <Column ss:AutoFitWidth="0" ss:Width="72"/>
	   <Column ss:AutoFitWidth="0" ss:Width="79.5"/>
	   <Column ss:AutoFitWidth="0" ss:Width="79.5"/>
	  <#list map?keys as region>
		   <Row ss:Height="40.5">
			<Cell ss:MergeAcross="5" ss:StyleID="s65"><Data ss:Type="String">${region}</Data></Cell>
		   </Row>
		   <Row ss:AutoFitHeight="0" ss:Height="31.5">
			<Cell ss:StyleID="s16"><Data ss:Type="String">地区</Data></Cell>
			<Cell ss:StyleID="s16"><Data ss:Type="String">品名</Data></Cell>
			<Cell ss:StyleID="s16"><Data ss:Type="String">价格区间</Data></Cell>
			<Cell ss:StyleID="s17"><Data ss:Type="String">涨跌</Data></Cell>
			<Cell ss:StyleID="s16"><Data ss:Type="String">备注</Data></Cell>
			<Cell ss:StyleID="s16"><Data ss:Type="String">类型</Data></Cell>
		   </Row>
			<#list map[region] as excel>
			   <Row ss:AutoFitHeight="0" ss:Height="29.25">
				<Cell ss:StyleID="s18"><Data ss:Type="String">${excel.distract!''}</Data></Cell>
				<Cell ss:StyleID="s18"><Data ss:Type="String">${excel.productName!''}</Data></Cell>
			<#if "停收停报看货给价价格电议暂不报价暂不采购暂无报价"?index_of(excel.price) gt -1>
					<Cell ss:StyleID="s77"><Data ss:Type="String">${excel.price!''}</Data></Cell>
				<#else>
					<Cell ss:StyleID="s18"><Data ss:Type="String">${excel.price!''}</Data></Cell>
				</#if>
				<#if excel.upDown?index_of("↑") gt -1>
					<Cell ss:StyleID="s77"><Data ss:Type="String">${excel.upDown!''}</Data></Cell>
				<#elseif excel.upDown?index_of("↓") gt -1>
					<Cell ss:StyleID="s76"><Data ss:Type="String">${excel.upDown!''}</Data></Cell>
				<#elseif "停收停报看货给价价格电议暂不报价暂不采购暂无报价"?index_of(excel.price) gt -1>
					<Cell ss:StyleID="s18"><Data ss:Type="String">${'-'}</Data></Cell>
				<#else>
					<Cell ss:StyleID="s18"><Data ss:Type="String">${excel.upDown!''}</Data></Cell>
				</#if>
				<Cell ss:StyleID="s18"><Data ss:Type="String">${excel.tax!''}</Data></Cell>
				<Cell ss:StyleID="s18"><Data ss:Type="String">${excel.category!''}</Data></Cell>
				<Cell ss:StyleID="s77"><Data ss:Type="String">${excel_index+1}</Data></Cell>
			   </Row>
		 	</#list>
	   </#list>
	  </Table>
  <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
   <PageSetup>
    <Header x:Margin="0.3"/>
    <Footer x:Margin="0.3"/>
    <PageMargins x:Bottom="0.75" x:Left="0.7" x:Right="0.7" x:Top="0.75"/>
   </PageSetup>
   <Print>
    <ValidPrinterInfo/>
    <PaperSizeIndex>9</PaperSizeIndex>
    <HorizontalResolution>600</HorizontalResolution>
    <VerticalResolution>600</VerticalResolution>
   </Print>
   <Selected/>
   <Panes>
    <Pane>
     <Number>3</Number>
     <RangeSelection>R1C1:R3C6</RangeSelection>
    </Pane>
   </Panes>
   <ProtectObjects>False</ProtectObjects>
   <ProtectScenarios>False</ProtectScenarios>
  </WorksheetOptions>
 </Worksheet>
</Workbook>
