$(function(){
	
	var localCacheUrl = "/steel/excel/steelExcelListByCache";
	var crawlAllDataUrl = "/steel/excel/steelExcelListByCrawl";
	var editUrl = "/steel/excel/saveSteel";
	var exportWordUrl = "/steel/excel/exportSteelExcel";
	var clearCrawlUrl = "/steel/excel/clearSteelCrawlCache";
	var exchangeUrl = "/steel/excel/exchange";
	
		
	datagridObj.createDataGrid("steelExcel",localCacheUrl,crawlAllDataUrl,editUrl,exportWordUrl,clearCrawlUrl,exchangeUrl);
	datagridObj.createLoadingMsg("steelExcel");
	datagridObj.createPagination("steelExcel",localCacheUrl);
})