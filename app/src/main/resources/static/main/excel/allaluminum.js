$(function(){
	
	var localCacheUrl = "/allaluminum/excel/allaluminumExcelListByCache";
	var crawlAllDataUrl = "/allaluminum/excel/allaluminumExcelListByCrawl";
	var editUrl = "/allaluminum/excel/saveAllaluminum";
	var exportWordUrl = "/allaluminum/excel/exportAllaluminumExcel";
	var clearCrawlUrl = "/allaluminum/excel/clearAllaluminumCrawlCache";
	var exchangeUrl = "/allaluminum/excel/exchange";
	
		
	datagridObj.createDataGrid("AllaluminumExcel",localCacheUrl,crawlAllDataUrl,editUrl,exportWordUrl,clearCrawlUrl,exchangeUrl);
	datagridObj.createLoadingMsg("AllaluminumExcel");
	datagridObj.createPagination("AllaluminumExcel",localCacheUrl);
})