var editRow = null; //编辑标识


var datagridObj = {
		createDataGrid: function(tableId,localCacheUrl,crawlAllDataUrl,editUrl,exportWordUrl,clearCrawlUrl){
			var datagrid = $("#"+tableId);
			datagrid.datagrid({
			    columns:[[
			    	{field : 'ck',checkbox : true},
					{field:'id',title:'id',hidden:true},
					{field:'title',title:'标题',width:200},
					{field:'description',title:'内容',width:700,align:'left'},
					{field:'region',title:'地区',width:100,align:'center',
						editor:{
							type:'text',//文本类型
							options: {
								required: true //是必填项
							}
						}
					},
					{field:'dateTime',title:'时间',width:150,align:'center'}
			    ]],
			    loadMsg:"正在加载数据,请稍等!",
			    pagination:true,
			    rownumbers:true,
			    pageSize:20,
			    pageNumber:1,
			    pageList:[10,20,30,40,50],
			    toolbar: [{
			    	text: '加载缓存数据',
					iconCls: 'icon-reload',
					handler: function(){
						getLocalCache(tableId,localCacheUrl);
					}
				},'-',{
					text: '爬取全部数据',
					iconCls: 'icon-reload',
					handler: function(){
						getCrawlAllData(tableId,crawlAllDataUrl);
					}
				},'-',{
					text: '导出word',
					iconCls: 'icon-print',
					handler: function(){
						exportWord(tableId,exportWordUrl);
					}
				},'-',{
					text: '清除爬虫标识',
					iconCls: 'icon-cancel',
					handler: function(){
						clearCrawl(clearCrawlUrl);
					}
				}],
				onDblClickRow:function(rowIndex, field, value){
					editRow = rowIndex;
					datagrid.datagrid("beginEdit", rowIndex);
					var ed = datagrid.datagrid('getEditor', {index:editRow,field:field});
			        $(ed.target).focus();
				},
				onClickRow:function(rowIndex, rowData){
					if(rowIndex != editRow){
						datagrid.datagrid("endEdit", editRow);
					}
				},
				onAfterEdit:function(rowIndex, rowData){
					if(editRow != null){
						//获取修改行的数据
						var rows = datagrid.datagrid('getRows'); 
						var row = rows[editRow];
						editRow = null;
						$.ajax({
					        type: "POST",
					        url: editUrl,
					        data:{"id":row.id,"region":row.region},
					        dataType: "json",
					        success: function(data){
					        	if(data.code == 200){
					        		alert(data.msg);
					        	}else{
					        		alert(data.msg);
					        	}
					        	datagrid.datagrid("loaded");
					        }
					    });
					}
				}
			
			});	
		},
		createLoadingMsg:function(tableId){
			var datagrid = $("#"+tableId);
			$.extend($.fn.datagrid.methods, {  
		        //显示遮罩  
		        loading: function (jq, msg) {  
		            return jq.each(function () {  
		                var panel = datagrid.datagrid("getPanel");  
		                if (msg == undefined) {  
		                    msg = "正在加载数据，请稍候...";  
		                }  
		                $("<div class=\"datagrid-mask\"></div>").css({ display: "block", width: panel.width(), height: panel.height() }).appendTo(panel);  
		                $("<div class=\"datagrid-mask-msg\"></div>").html(msg).appendTo(panel).css({ display: "block", left: (panel.width() - $("div.datagrid-mask-msg", panel).outerWidth()) / 2, top: (panel.height() - $("div.datagrid-mask-msg", panel).outerHeight()) / 2 });  
		            });  
		        }  
		,  
		        //隐藏遮罩  
		        loaded: function (jq) {  
		            return jq.each(function () {  
		                var panel = datagrid.datagrid("getPanel");  
		                panel.find("div.datagrid-mask-msg").remove();  
		                panel.find("div.datagrid-mask").remove();  
		            });  
		        }  
		    });
		}
}

function getLocalCache(tableId,url){
	var datagrid = $("#"+tableId);
	datagrid.datagrid("loading","数据正在加载!");
	$.ajax({
        type: "GET",
        url: url,
        dataType: "json",
        success: function(data){
        	if(data.code == 200){
        		if(data.t != null){
        			datagrid.datagrid("loadData",data.t);
        		}
        		alert(data.msg);
        	}else{
        		alert(data.msg);
        	}
        	datagrid.datagrid("loaded");
        }
    });
}

function getCrawlAllData(tableId,url){
	$.messager.confirm('确认对话框','确认要爬取今日所有数据吗?',function(r){
	    if (r){
	    	var datagrid = $("#"+tableId);
	    	datagrid.datagrid("loading","数据正在加载!");
	    	$.ajax({
	            type: "GET",
	            url: url,
	            dataType: "json",
	            success: function(data){
	            	if(data.code == 200){
	            		if(data.t != null){
	            			datagrid.datagrid("loadData",data.t);
	            		}
	            	}else{
	            		alert(data.msg);
	            	}
	            	datagrid.datagrid("loaded");
	            }
	        });
	    }
	});	
}

function exportWord(tableId,url){
	$.messager.confirm('确认对话框','确认要导出选中的数据吗?',function(r){
		if(r){
			var datagrid = $("#"+tableId);
			var selected = datagrid.datagrid("getSelections");
			var selectedId = new Array();
			for(var i=0;i<selected.length;i++){
				selectedId.push(selected[i].id);
			}
			if(selectedId.length == 0){
				alert("请选择要导出的数据!");
				return false;
			}
			window.open(url+"?id="+selectedId.join(","));
		}
	});
}


function clearCrawl(url){
	$.messager.confirm('确认对话框','确认要清楚缓存标识吗?',function(r){
	    if (r){
	    	$.ajax({
	            type: "GET",
	            url: url,
	            dataType: "json",
	            success: function(data){
	            	alert(data.msg);
	            }
	        });
	    }
	});	
}