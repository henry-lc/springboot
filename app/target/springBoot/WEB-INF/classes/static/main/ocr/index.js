
$(function(){
	var localCacheUrl = "/paper/image/load";
	var clearCacheUrl = "/paper/image/clearImageCache";
	var deleteCacheUrl = "/paper/image/deleteById";
	var exportWordUrl = "/paper/image/exportPaper";
	$('#paperTable').datagrid({
	    columns:[[
	    	{field : 'ck',checkbox : true},
	    	{field:'id',title:'ID',width:100,hidden:true},
			{field:'description',title:'识别内容',width:700},
			{field:'region',title:'区域',width:100}
	    ]],
	    toolbar: [{
	    	text: '加载缓存数据',
			iconCls: 'icon-reload',
			handler: function(){
				getLocalCache(localCacheUrl);
			}
		},'-',{
			text: '清空缓存数据',
			iconCls: 'icon-reload',
			handler: function(){
				clearCrawl(clearCacheUrl,localCacheUrl);
			}
		},'-',{
			text: '删除数据',
			iconCls: 'icon-remove',
			handler: function(){
				del(deleteCacheUrl,localCacheUrl);
			}
		},'-',{
			text: '导出word',
			iconCls: 'icon-print',
			handler: function(){
				exportWord(exportWordUrl);
			}
		}
		]
	});
	var datagrid = $('#paperTable');
	$.extend($.fn.datagrid.methods, {  
        //显示遮罩  
        loading: function (jq, msg) {  
            return jq.each(function () {  
                var panel = datagrid.datagrid("getPanel");  
                if (msg == undefined) {  
                    msg = "正在加载数据，请稍候...";  
                }  
                $("<div class=\"datagrid-mask\"></div>").css({ display: "block", width: panel.width(), height: panel.height() }).appendTo(panel);  
                $("<div class=\"datagrid-mask-msg\"></div>").html(msg).appendTo(panel).css({ display: "block", left: (panel.width() - $("div.datagrid-mask-msg", panel).outerWidth()) / 2, top: (panel.height() - $("div.datagrid-mask-msg", panel).outerHeight()) / 2 });  
            });  
        }  
,  
        //隐藏遮罩  
        loaded: function (jq) {  
            return jq.each(function () {  
                var panel = datagrid.datagrid("getPanel");  
                panel.find("div.datagrid-mask-msg").remove();  
                panel.find("div.datagrid-mask").remove();  
            });  
        }  
    });
	
})

function exportWord(url){
	$.messager.confirm('确认对话框','确认要导出选中的数据吗?',function(r){
		if(r){
			var datagrid = $("#paperTable");
			var selected = datagrid.datagrid("getSelections");
			var selectedId = new Array();
			for(var i=0;i<selected.length;i++){
				selectedId.push(selected[i].id);
			}
			if(selectedId.length == 0){
				alert("请选择要导出的数据!");
				return false;
			}
			window.open(url+"?id="+selectedId.join(","));
		}
	});
}

function getLocalCache(url){
	var datagrid = $("#paperTable");
	datagrid.datagrid("loading","数据正在加载!");
	$.ajax({
        type: "GET",
        url: url,
        dataType: "json",
        success: function(data){
        	if(data.code == 200){
        		if(data.t != null){
        			datagrid.datagrid("loadData",data.t);
        		}
        	}else{
        		alert(data.msg);
        	}
        	datagrid.datagrid("loaded");
        }
    });
}

function clearCrawl(url,localCacheUrl){
	$.messager.confirm('确认对话框','确认要清除缓存标识吗?',function(r){
		$("#paperTable").datagrid("loadData",{total:0,rows:[]});
	    if (r){
	    	$.ajax({
	            type: "GET",
	            url: url,
	            dataType: "json",
	            success: function(data){
	            	alert(data.msg);
	            }
	        });
	    }
	});	
}

function del(url,localCacheUrl){
	$.messager.confirm('确认对话框','确认要删除选中的数据吗?',function(r){
		if(r){
			var datagrid = $("#paperTable");
			var selected = datagrid.datagrid("getSelections");
			var selectedId = new Array();
			for(var i=0;i<selected.length;i++){
				selectedId.push(selected[i].id);
			}
			if(selectedId.length == 0){
				alert("请选择要删除的数据!");
				return false;
			}
			$.ajax({
	            type: "GET",
	            url: url,
	            dataType: "json",
	            data:{"id":selectedId.join(",")},
	            success: function(data){
	            	alert(data.msg);
	            	datagrid.datagrid("loadData",{total:0,rows:[]});
	            	getLocalCache(localCacheUrl);
	            }
	        });
		}
	});
}
