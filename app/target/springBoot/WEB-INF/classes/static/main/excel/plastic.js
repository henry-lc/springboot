$(function(){
	
	var localCacheUrl = "/plastic/excel/plasticExcelListByCache";
	var crawlAllDataUrl = "/plastic/excel/plasticExcelListByCrawl";
	var exportWordUrl = "/plastic/excel/exportPlasticExcel";
	var clearCrawlUrl = "/plastic/excel/clearPlasticCrawlCache";
	
		
	datagridObj.createDataGrid("plasticTable",localCacheUrl,crawlAllDataUrl,exportWordUrl,clearCrawlUrl);
	datagridObj.createLoadingMsg("plasticTable");
	datagridObj.createPagination("plasticTable",localCacheUrl);
})