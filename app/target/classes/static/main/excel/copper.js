$(function(){
	
	var localCacheUrl = "/copper/excel/copperExcelListByCache";
	var crawlAllDataUrl = "/copper/excel/copperExcelListByCrawl";
	var editUrl = "/copper/excel/saveCopper";
	var exportWordUrl = "/copper/excel/exportCopperExcel";
	var clearCrawlUrl = "/copper/excel/clearCopperCrawlCache";
	var exchangeUrl = "/copper/excel/exchange";
	
		
	datagridObj.createDataGrid("copperExcel",localCacheUrl,crawlAllDataUrl,editUrl,exportWordUrl,clearCrawlUrl,exchangeUrl);
	datagridObj.createLoadingMsg("copperExcel");
	datagridObj.createPagination("copperExcel",localCacheUrl);
})