$(function(){
	var localCacheUrl = "/steel/word/getSteelBaseList";
	var crawlAllDataUrl = "/steel/word/getSteelBaseCrawl";
	var editUrl = "/steel/word/saveBase";
	var exportWordUrl = "/steel/word/exportBase";
	var clearCrawlUrl = "/steel/word/clearBaseCrawlCache";
	
	datagridObj.createDataGrid("steelBase",localCacheUrl,crawlAllDataUrl,editUrl,exportWordUrl,clearCrawlUrl);
	datagridObj.createLoadingMsg("steelBase");
})